export ZSH="$HOME/.config/zsh"

# History
export HISTFILE="$ZSH/.zhistory"    # History filepath
export HISTSIZE=10000                   # Maximum events for internal history
export SAVEHIST=10000                   # Maximum events in history file
setopt HIST_IGNORE_ALL_DUPS
# # remove dangerous entries from the shell history
temp_histfile="/tmp/$$.temp_histfile"
# `rm .*-r` for deleting directories
# `nmcli .* password [^$]` for deleting login to wifi without variable
rg -v -N '^: [0-9]*:[0-9]*;(rm .*-r|nmcli .* password [^$])' $HISTFILE > $temp_histfile
mv $temp_histfile $HISTFILE

# Completion
autoload -Uz compinit; compinit
setopt completealiases
_comp_options+=(globdots)
zmodload zsh/complist
zstyle ':completion:*' menu select
# faster paste? can't remember
zstyle ':bracketed-paste-magic' active-widgets '.self-*'
# bash-completion (mainly for expressvpn)
autoload -Uz bashcompinit; bashcompinit
source ~/.config/zsh/bash-completion/completions/expressvpn

# stack
setopt AUTO_PUSHD           # Push the current directory visited on the stack.
setopt PUSHD_IGNORE_DUPS    # Do not store duplicates in the stack.
setopt PUSHD_SILENT         # Do not print the directory stack after pushd or popd.

# plugin settings
ZSH_ALIAS_FINDER_AUTOMATIC=true

# load plugins
# TODO lazily?
source "$ZSH/plugins/alias-finder.zsh"
source "$ZSH/plugins/git.zsh"
source "$ZSH/plugins/spectrum.zsh"
source "$ZSH/plugins/timer.zsh"
source "$ZSH/plugins/vi-mode.zsh"
source "$ZSH/plugins/virtualenv.zsh"
source "$ZSH/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
source "$ZSH/plugins/zsh-bd/bd.zsh"
source "$ZSH/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
source "$ZSH/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
source "$ZSH/plugins/nix-zsh-completions/nix.plugin.zsh"
prompt_nix_shell_setup
source "$ZSH/plugins/zsh-nix-shell/nix-shell.plugin.zsh"

source "$ZSH/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh"
export HISTORY_SUBSTRING_SEARCH_PREFIXED=true
bindkey '^[OA' history-substring-search-up
bindkey '^[OB' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

fzf_key_binding=/usr/share/fzf/key-bindings.zsh
fzf_completion=/usr/share/fzf/completion.zsh
[ -f $fzf_key_binding ] && source $fzf_key_binding
[ -f $fzf_completion ] && source $fzf_completion

# Prompt theme
source "$ZSH/themes/muse-gruvbox.zsh"

SETUP_DIR=~/.config/zsh/setup
for file in $SETUP_DIR/*
    source $file
LOCAL_FILE=$SETUP_DIR/local.sh
if [[ -e $LOCAL_FILE ]]; then
    source $LOCAL_FILE  # source last to override
fi

# vi mode
# text objects
# ref https://thevaluable.dev/zsh-install-configure-mouseless/
autoload -Uz select-bracketed select-quoted
zle -N select-quoted
zle -N select-bracketed
for km in viopp visual; do
  bindkey -M $km -- '-' vi-up-line-or-history
  for c in {a,i}${(s..)^:-\'\"\`\|,./:;=+@}; do
    bindkey -M $km $c select-quoted
  done
  for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
    bindkey -M $km $c select-bracketed
  done
done

# surround
autoload -Uz surround
zle -N delete-surround surround
zle -N add-surround surround
zle -N change-surround surround
bindkey -M vicmd sr change-surround
bindkey -M vicmd sd delete-surround
bindkey -M vicmd sa add-surround
bindkey -M visual sa add-surround

# Some external stuff
# Broot
br=~/.config/broot/launcher/bash/br
[ -f $br ] && source $br

# Zoxide
eval "$(zoxide init zsh)"

# remove duplicates
typeset -aU path
