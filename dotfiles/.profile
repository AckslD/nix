# vim: filetype=sh

# Start dunst and low battery notification
dunst &
batsignal -b -I /usr/share/icons/Adwaita/256x256/legacy/battery.png

# pcloud
gtk-launch pcloud
