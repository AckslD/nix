# vim: filetype=zsh
export PATH=~/.local/bin:$PATH

# make sure variables (and local) are present globally
SETUP_DIR=~/.config/zsh/setup
LOCAL_FILE=$SETUP_DIR/local.sh
source $SETUP_DIR/variables.sh
if [[ -e $LOCAL_FILE ]]; then
    source $LOCAL_FILE
fi

# pyenv
if hash pyenv 2> /dev/null; then
    # TODO should (pyenv init -) be used?
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init --path)"
fi

# pick display server to start (if any)
function start() {
    start_wm=~/.config/x/start_wm
    while true; do
        start=$(echo "$WM_CHOICES" | fzf --prompt "Start: " --height=5)
        case $start in
            herbstluftwm*)
                echo "Starting X with herbstluftwm..."
                ln -sf ~/.config/x/start_herbstluftwm $start_wm
                startx
                rm $start_wm
                echo "Bye X!"
                ;;
            awesome*)
                echo "Starting X with awesome..."
                ln -sf ~/.config/x/start_awesome $start_wm
                startx
                rm $start_wm
                echo "Bye X!"
                ;;
            sway*)
                echo "Starting sway..."
                export XDG_CURRENT_DESKTOP=sway
                sway
                echo "Bye sway!"
                ;;
            *)
                break
                ;;
        esac
    done
}
if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
    start
fi
