import os
if 'PYTHONNORICH' not in os.environ:
    try:
        from rich.traceback import install
        install(show_locals=True)
    except ModuleNotFoundError:
        pass


from typing import Optional
from time import perf_counter


class TaskTimer:
    def __init__(self):
        self.last_task: Optional[dict] = None

    def __call__(self, name: str = ''):
        now = perf_counter()
        if self.last_task:
            print(f'END ({now - self.last_task["start"]:.4f}s): {self.last_task["name"]}')
        if not name:
            self.last_task = None
            return
        print(f'BEGIN: {name}')
        self.last_task = {'name': name, 'start': now}


class AckslD:
    TaskTimer = TaskTimer


import sys
sys.modules['acksld'] = AckslD
