#!/usr/bin/env python3
import gi
gi.require_version('Playerctl', '2.0')
from gi.repository import Playerctl, GLib


def on_metadata(player, metadata):
    if 'xesam:artist' in metadata.keys() and 'xesam:title' in metadata.keys():
        print('Now playing:')
        print('{artist} - {title}'.format(
            artist=metadata['xesam:artist'][0], title=metadata['xesam:title']))


def on_play(player, status):
    print('Playing at volume {}'.format(player.props.volume))


def on_pause(player, status):
    print('Paused the song: {}'.format(player.get_title()))


manager = Playerctl.PlayerManager()
spotifyd = manager.props.player_names[0]
player = Playerctl.Player.new_from_name(spotifyd)
print(player.props.player_name)

player.connect('playback_status', on_play)
player.connect('playback-status', on_play)
player.connect('playback-status::playing', on_play)
player.connect('playback-status::paused', on_pause)
player.connect('playback-status::Playing', on_play)
player.connect('playback-status::Paused', on_pause)
player.connect('metadata', on_metadata)

# wait for events
main = GLib.MainLoop()
main.run()
