#!/usr/bin/env bash
QUERY_CMD="swaymsg -t get_inputs -r | jq '.[] | select(.type==\"touch\") | .libinput.send_events'"
APP=fingertouch
ON_LABEL="\"enabled\""
~/.config/waybar/scripts/toggle-button.sh "$QUERY_CMD" "$APP" "$ON_LABEL"
