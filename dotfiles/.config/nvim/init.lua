require('acksld.funcs.global')
R('funcs.treesitter').disable_large_files()
R('settings')
R('lazy')
R('keys')
if vim.g.neovide then
  R('neovide')
end
R('autocmds')
R('commands')
