vim.filetype.add({
  extension = {
    vifm = 'vim',
    dot = 'dot',
    sage = 'sage',
    ino = 'arduino',
    jinja = 'jinja',
    jinja2 = 'jinja',
  },
  filename = {
    vifmrc = 'vim',
  },
})
