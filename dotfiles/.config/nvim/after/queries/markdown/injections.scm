;; extends

(fenced_code_block
  (info_string
    (language) @_lang (#eq? @_lang "code-cell"))
  (code_fence_content) @injection.content
  (#set! injection.language "python"))
