local brj = R('funcs.bracket_jump')

local function set_keymap(mode, opts, keymaps)
  for _, keymap in ipairs(keymaps) do
    vim.keymap.set(mode, keymap[1], keymap[2], vim.tbl_extend('force', opts, keymap[3] or {}))
  end
end

local function clear()
  vim.cmd('nohlsearch')
  vim.lsp.buf.clear_references()
  IfHas('hlslens.main', function(hlslens) hlslens.stop() end)
  IfHas('notify', function(notify) notify.dismiss() end)
end

vim.keymap.set('n', '<Esc>', clear, {desc = 'clear'})
vim.keymap.set('n', '<Left>', brj.prev, {desc = 'prev ([)'})
vim.keymap.set('n', '<Right>', brj.next, {desc = 'next (])'})

-- normal {{{1
set_keymap('n', {noremap=true, silent=true}, {
  -- execute q macro
  {'Q', '@q'},

  -- yank to end of line
  {'Y', 'y$'},

  -- yank/paste clipboard
  {'gy', '"+y'},
  {'gp', '"+p'},
  {'gP', '"+P'},
  {'gY', '"+y$'},

  -- reselect pasted text
  {'gV', '`[v`]'},

  -- source config
  {'<C-s>', ':lua R("funcs.config").source()<CR>'},

  -- Jump list
  {'[j', '<C-o>'},
  {']j', '<C-i>'},

  -- Smart way to move between windows
  {'<C-h>', '<C-w>h'},
  {'<C-j>', '<C-w>j'},
  {'<C-k>', '<C-w>k'},
  {'<C-l>', '<C-w>l'},

  -- Page down/up
  {'[d', '<PageUp>'},
  {']d', '<PageDown>'},

  -- Smart way to move between tabs
  {'<A-h>', 'gT'},
  {'<A-l>', 'gt'},

  -- Resize split
  {'<S-Up>', ':resize -2<CR>'},
  {'<S-Down>', ':resize +2<CR>'},
  {'<S-Left>', ':vertical resize -2<CR>'},
  {'<S-Right>', ':vertical resize +2<CR>'},

  -- Quickfix
  {'<Up>', ':copen<CR>'},
  {'<Down>', ':cclose<CR>'},
  {'[q', ':cprevious<CR>'},
  {']q', ':cnext<CR>'},

  -- Navigate buffers
  {']b', '<Cmd>bnext<CR>'},
  {'[b', '<Cmd>bprev<CR>'},
  {']n', '<Cmd>next<CR>'},
  {'[n', '<Cmd>prev<CR>'},

  -- jump diagnostic
  {'[g', "<Cmd>lua vim.diagnostic.goto_prev({float = true})<CR>"},
  {']g', "<Cmd>lua vim.diagnostic.goto_next({float = true})<CR>"},

  -- fix spelling with first suggestion
  {'gz', 'z=1<CR><CR>'},

  {'cg*', '*Ncgn'},
  {'g.', [[/\V<C-r>"<CR>cgn<C-a><Esc>]]},

  -- center search result
  {'n', 'nzzzv'},
  {'N', 'Nzzzv'},

  -- vifm
  {'<C-n>', function() R('funcs.vifm').select_and_edit() end, {desc = 'vifm'}},

  -- indentation
  {'g=', function()
    R('funcs.keep').cursor_wrap(function()
      print('indenting')
      vim.cmd.normal('gg=G')
    end)()
  end, {desc = 'fix indentation'}},
})

-- visual {{{1
set_keymap('x', {noremap=true, silent=true}, {
  -- Allow pasting same thing many times
  {'p', '""p:let @"=@0<CR>'},

  -- better indent
  {'<', '<gv'},
  {'>', '>gv'},

  -- yank/paste clipboard
  {'gy', '"+y'},
  {'gp', '"+p'},

  -- Visual mode pressing * or # searches for the current selection
  -- {'*', ':<C-u>lua R("funcs.search").visual_selection("/")<CR>/<C-r>=@/<CR><CR>'},
  -- {'#', ':<C-u>lua R("funcs.search").visual_selection("?")<CR>?<C-r>=@/<CR><CR>'},

  -- move selected line(s)
  {'K', ':move \'<-2<CR>gv-gv'},
  {'J', ':move \'>+1<CR>gv-gv'},

  -- repeat dot
  {'.', '<Cmd>normal .<CR>'},
  -- repeat macro
  {'@', '<Cmb>normal Q<CR>'},
})

-- insert {{{1
set_keymap('i', {noremap=true, silent=true}, {
  -- Smart way to move between tabs
  {'<A-h>', [[<C-\><C-n>gT]]},
  {'<A-l>', [[<C-\><C-n>gt]]},

  -- alternative esc
  {'jk', '<Esc>'},

  -- insert special carachters
  {'<C-b>', '<C-k>'},

  -- navigate display lines
  {'<Down>', '<Esc>gja'},
  {'<Up>', '<Esc>gka'},
})
-- set_keymap('i', {noremap=true, silent=true, expr=true}, {
--     -- Completion
--     {'<Tab>', [[pumvisible() ? "\<C-n>" : "\<C-x>\<C-o>"]]},
-- })

-- terminal {{{1
set_keymap('t', {noremap=true, silent=true}, {
  -- paste
  {'<C-v>', [[<C-\><C-n>pi]]},
  -- quickfix from buffer
  {'<C-q>', [[<C-\><C-n>:lua R("funcs.quicklist").create_from_buffer()<CR>]]},

  -- escape in terminal
  {'<Esc>', [[<C-\><C-n>]]},
  {[[<M-\>]], '<Esc>'},

  -- Smart way to move between windows
  {'<C-h>', [[<C-\><C-N><C-w>h]]},
  {'<C-j>', [[<C-\><C-N><C-w>j]]},
  {'<C-k>', [[<C-\><C-N><C-w>k]]},
  {'<C-l>', [[<C-\><C-N><C-w>l]]},

  -- Smart way to move between tabs
  {'<A-h>', [[<C-\><C-N>gT]]},
  {'<A-l>', [[<C-\><C-N>gt]]},
})

local status, wk = pcall(require, "which-key")
if not (status) then
  vim.notify("couldn't load which-key, skipping mappings")
  return
end

-- normal {{{1
local which_key_map = {}
-- general
which_key_map['w'] = {'<Cmd>w<CR>', 'save file'}
which_key_map['W'] = {'<Cmd>wa<CR>', 'save all'}
which_key_map['x'] = {'<Cmd>q<CR>', 'quit file'}
which_key_map['X'] = {'<Cmd>qa<CR>', 'quit all'}
which_key_map['Q'] = {'<Cmd>wa<CR><Cmd>qa<CR>', 'save quit all'}
which_key_map['S'] = {
  string.format('<Cmd>wa<CR><Cmd>mksession! %s/Session.vim<CR><Cmd>qa<CR>', vim.fn.getcwd()),
  'save session quit all',
}
which_key_map['<CR>'] = {':noh<CR>', 'no search hl'}
which_key_map['*'] = {'*<Cmd>lua R("funcs.search").vim("*."..vim.fn.expand("%:e"))<CR>', 'vimgrep cursor'}

-- bracket jump
local brackets = {}
for _, char in ipairs({'q', 'g', 's', 'n', 'b'}) do
  brackets[char] = {function() brj.set(char) end, string.format('set bracket jump (%s)', char)}
end
brackets.name = '+bracket jumps'
which_key_map.o = brackets

-- buffers
which_key_map.b = {
  name = '+buffer',
  d = {':Bclose<CR>:tabclose<CR>gT', 'close'},
  a = {':bufdo bd<CR>', 'do'},
  y = {'<Cmd>let @+=expand("%:p")<CR>', 'yank path'},
}

-- tabs
which_key_map.t = {
  name = '+tabs',
  n = {':tabnew<CR>', 'new'},
  t = {':tabnew %<CR><C-o>', 'new current'},
  N = {'<Cmd>lua R("funcs.temp").new()<CR>', 'new temp'},
  o = {':tabonly<CR>', 'close others'},
  c = {':tabclose<CR>', 'close'},
  m = {':tabmove ', 'move'},
  e = {':tabedit <C-r>=expand("%:p:h")<CR>/', 'edit cwd'},
  s = {'<Cmd>mksession!<CR>', 'save session'},
  l = {'<Cmd>source Session.vim<CR>', 'load session'},
  p = {function()
    vim.ui.select(vim.api.nvim_list_tabpages(), {
      prompt = 'Select tab',
      format_item = function(item)
        local tab = R('funcs.tabs').get_tab(item)
        return string.format('%s: %s (%s)', item, tab.name, tab.cwd)
      end,
    }, function(choice)
        if choice == nil then
          return
        end
        vim.api.nvim_set_current_tabpage(choice)
      end)
  end, 'pick tab'},
}

-- quickfix
which_key_map.q = {
  name = '+quickfix',
  q = {':lua R("funcs.quicklist").create_from_buffer()<CR>', 'from buffer'},
  e = {':lua R("funcs.quicklist").estream_from_buffer()<CR>', 'from buffer'},
  G = {':clast<CR>', 'last'},
  g = {':cfirst<CR>', 'first'},
  c = {':cclose<CR>', 'close'},
  o = {':copen<CR>', 'open'},
}

-- make
which_key_map.m = {
  name = '+make',
  m = {function() R("funcs.terminal").dedicated("make", {termname = 'make'}) end, 'make'},
  c = {function() R("funcs.terminal").dedicated("make clean", {termname = 'make'}) end, 'clean'},
  a = {function() R("funcs.terminal").dedicated("make all", {termname = 'make'}) end, 'all'},
  o = {function() R("funcs.terminal").dedicated("make open", {termname = 'make'}) end, 'open'},
}

-- search
which_key_map.s = {
  name = '+search',
  f = {':lua R("funcs.search").vim("*."..vim.fn.expand("%:e"))<CR>', 'search project'},
  a = {':lua R("funcs.search").vim("*")<CR>', 'seach project all'},
  s = {":set operatorfunc=v:lua.require'acksld.funcs.grep'.operator<CR>g@", 'grep operator'},
}

-- config
which_key_map.c = {
  name = '+config',
  s = {
    name = '+luasnip',
    l = {
      function()
        package.loaded['acksld.plugins.luasnip'] = nil
        IfHas('acksld.plugins.luasnip', function(m)
          m.config()
        end)
      end,
      'reload snippets',
    },
    e = {'<Cmd>edit ~/dev/config/nvim/lua/acksld/plugins/luasnip.lua<CR>', 'edit snippets'},
  }
}

-- cwd
which_key_map.n = {
  c = {
    name = '+cwd',
    d = {':cd %:p:h<CR>:pwd<CR>', 'cd to current'},
    l = {':lcd %:p:h<CR>:pwd<CR>', 'lcd to current'},
  },
}

-- unicode
which_key_map.u = {
  name = '+unicode/color',
  t = {'<Cmd>lua R("funcs.unicode").replace_hex_to_char()<CR>', 'hx2ch'},
  f = {'<Cmd>lua R("funcs.unicode").replace_char_to_hex()<CR>', 'ch2hx'},
  p = {'<Cmd>lua R("colors.colorscheme").pick()<CR>', 'pick colorscheme'},
  b = {'<Cmd>lua R("funcs.highlight").toggle_bright_comments()<CR>', 'toggle bright comments'},
}

-- input/language
which_key_map.i = {
  name = '+input/language',
  s = {'<Cmd>lua R("funcs.language").toggle_swedish()<CR>', 'toggle sv'},
  c = {'<Cmd>lua R("funcs.layout").toggle_colemak()<CR>', 'toggle colemak'},
  n = {'<Cmd>set spell!<CR>', 'toggle spell'},
}

-- plugin/profile
which_key_map.p = {
  name = 'plugin',
  r = {'<Cmd>lua R("plugins")()<CR>', 'load plugins conf'},
  c = {'<Cmd>lua R("funcs.config").compile()<CR>', 'packer compile'},
  w = {function() R('funcs.plugins').workon() end, 'work-on'},
  f = {function() R('funcs.plugins').find_file() end, 'find plugin file'},
  l = {function() R('funcs.plugins').find_line() end, 'find plugin line'},
  p = {
    name = 'profile',
    s = {'<Cmd>ProfStart<CR>', 'profile start'},
    e = {'<Cmd>ProfStop<CR>', 'profile stop'},
  },
}

-- navigate
which_key_map.n = {
  name = 'navigation',
  x = {'<C-^>', 'alternative file'},
}

-- lsp
which_key_map.l = {
  name = 'lsp',
  -- show diagnostics
  d = {"<Cmd>lua vim.diagnostic.open_float(0, {scope='line', source=true})<CR>", 'diagnostics'},
  -- toggle diagnostics
  v = {'<Cmd>lua vim.diagnostic.toggle_all()<CR>', 'toggle diagnostics all'},
  -- toggle bright comments
  Q = {"<Cmd>lua vim.diagnostic.setloclist()<CR>", 'set loclist'},
}


wk.register(which_key_map, {prefix = '<leader>'})

-- visual {{{1
local which_key_visual_map = {}
which_key_visual_map['K'] = {function()
  local _, start_line, start_col, _ = unpack(vim.fn.getpos('v'))
  local _, end_line, end_col, _ = unpack(vim.fn.getpos('.'))
  local text = vim.api.nvim_buf_get_text(0, start_line - 1, start_col - 1, end_line - 1, end_col, {})[1]
  vim.cmd('help ' .. text)
end, 'help'}

-- search
which_key_visual_map.s = {
  name = '+search',
  s = {':call v:lua.grep.operator(visualmode())<CR>', 'grep visual'},
}

wk.register(which_key_visual_map, {prefix = '<leader>', mode = 'v'})
