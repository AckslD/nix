return {
  'ziontee113/syntax-tree-surfer',
  keys = {
  -- NAVIGATION: Only change the keymap to your liking. I would not recommend changing anything about the .surf() parameters!
    {"<A-l>", '<Cmd>lua require("syntax-tree-surfer").surf("next", "visual")<CR>', mode = 'x', noremap = true, silent = true},
    {"<A-h>", '<Cmd>lua require("syntax-tree-surfer").surf("prev", "visual")<CR>', mode = 'x', noremap = true, silent = true},
    {"<A-k>", '<Cmd>lua require("syntax-tree-surfer").surf("parent", "visual")<CR>', mode = 'x', noremap = true, silent = true},
    {"<A-j>", '<Cmd>lua require("syntax-tree-surfer").surf("child", "visual")<CR>', mode = 'x', noremap = true, silent = true},

  -- SWAPPING WITH VISUAL SELECTION: Only change the keymap to your liking. Don't change the .surf() parameters!
  {"L", '<Cmd>lua require("syntax-tree-surfer").surf("next", "visual", true)<CR>', mode = 'x', noremap = true, silent = true},
  {"H", '<Cmd>lua require("syntax-tree-surfer").surf("prev", "visual", true)<CR>', mode = 'x', noremap = true, silent = true},
  },
  wkeys = {{
    r = {
      name = '+refactor',
      l = {'<Cmd>lua require("syntax-tree-surfer").move("n", false)<CR>', 'move down'},
      h = {'<Cmd>lua require("syntax-tree-surfer").move("n", true)<CR>', 'move up'},
      -- .select() will show you what you will be swapping with .move(), you'll get used to .select() and .move() behavior quite soon!
      x = {'<Cmd>lua require("syntax-tree-surfer").select()<CR>', 'select'},
      -- .select_current_node() will select the current node at your cursor
      c = {'<Cmd>lua require("syntax-tree-surfer").select_current_node()<CR>', 'select current'},
    },
  }, {prefix = '<leader>'}},
}
