return {
  'michaeljsmith/vim-indent-object',
  keys = {
    {'ai', mode = 'o'},
    {'ii', mode = 'o'},
    {'ai', mode = 'x'},
    {'ii', mode = 'x'},
    {'aI', mode = 'o'},
    {'iI', mode = 'o'},
    {'aI', mode = 'x'},
    {'iI', mode = 'x'},
  },
  dependencies = {'vim-textobj-user'},
}
