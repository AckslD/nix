return {
  'AckslD/nvim-trevJ.lua',
  lazy = true,
  init = function()
    vim.api.nvim_create_autocmd("FileType", {
      pattern = {
        "c",
        "cpp",
        "go",
        "html",
        "lua",
        "php",
        "python",
        "ruby",
        "rust",
        "r",
      },
      callback = function()
        vim.keymap.set('n', '<leader>j', function()
          require('trevj').format_at_cursor()
        end, {
            buffer = true,
          })
      end,
    })
  end,
}
