return {
  "andrewferrier/debugprint.nvim",
  lazy = true,
  wkeys = {
    {{
      ['/'] = {
        name = 'debugprint',
        p = {function() return require('debugprint').debugprint() end, 'plain'},
        P = {function() return require('debugprint').debugprint({ above = true }) end, 'plain above'},
        v = {function() return require('debugprint').debugprint({ variable = true }) end, 'variable'},
        V = {function() return require('debugprint').debugprint({ above = true, variable = true }) end, 'variable above'},
        o = {function() return require('debugprint').debugprint({ motion = true }) end, 'operator'},
        O = {function() return require('debugprint').debugprint({ above = true, motion = true }) end, 'operator above'},
      },
    }, {prefix = 'g', expr = true}},
    {{
      ['/'] = {
        name = 'debugprint',
        d = {function() require('debugprint').deleteprints() end, 'delete'},
      },
    }, {prefix = 'g'}},
  },
  opts = {
    create_keymaps = false,
  },
}
