return {
  'folke/noice.nvim',
  event = 'VeryLazy',
  dependencies = {
    'MunifTanjim/nui.nvim',
    'rcarriga/nvim-notify',
  },
  opts = {
    routes = {
      {
        view = "split",
        filter = { min_width = 500 },
      },
    },
  }
}
