return {
  'nanotee/zoxide.vim',
  cmd = {'Z', 'Lz'},
  dependencies = {'telescope-zoxide'},
  wkeys = {{
    n = {
      name = '+navigation',
      z = {'<Cmd>lua require("telescope").extensions.zoxide.list()<CR>', 'z list'},
    },
  }, {prefix = '<leader>'}},
}
