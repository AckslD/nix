return {
  "SmiteshP/nvim-navbuddy",
  dependencies = {
    'MunifTanjim/nui.nvim',
    'SmiteshP/nvim-navic',
  },
  lazy = true,
  config = {
    lsp = {
      preference = {
        'pyright',
      },
    },
  },
  wkeys = {{
    l = {
      n = {'<Cmd>lua require("nvim-navbuddy").open()<CR>', 'navbuddy'},
    },
  }, {prefix = '<leader>'}},
}
