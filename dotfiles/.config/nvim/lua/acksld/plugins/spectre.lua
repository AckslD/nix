return {
  'windwp/nvim-spectre',
  dependencies = {'popup.nvim', 'plenary.nvim'},
  lazy = true,
  wkeys = {{
    s = {
        name = '+search',
        p = {':lua require("spectre").open()<CR>', 'spectre'},
    },
  }, {prefix = '<leader>'}},
  config = true,
}
