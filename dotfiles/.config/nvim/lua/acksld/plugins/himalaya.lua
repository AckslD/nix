return {
  'soywod/himalaya',
  cmd = 'Himalaya',
  wkeys = {{
    b = {
      m = {'<Cmd>Himalaya<CR>', 'open'},
    }
  }, {prefix = '<leader>'}},
  init = function()
    vim.g.himalaya_complete_contact_cmd = "khard email --remove-first-line --parsable '%s'"
  end,
  config = function(plugin)
    vim.opt.rtp:append(plugin.dir .. "/vim")
    R('funcs.autocmds').create_grouped_autocmds({
      MyHimalaya = {
        FileType = {
          callback = function()
            WK({
              m = {'<Plug>(himalaya-mbox-input)', 'change mailbox'},
              p = {'<Plug>(himalaya-mbox-prev-page)', 'prev page'},
              n = {'<Plug>(himalaya-mbox-next-page)', 'next page'},
              w = {'<Plug>(himalaya-msg-write)', 'write'},
              R = {'<Plug>(himalaya-msg-reply)', 'reply'},
              r = {'<Plug>(himalaya-msg-reply-all)', 'reply all'},
              f = {'<Plug>(himalaya-msg-forward)', 'forward'},
              a = {'<Plug>(himalaya-msg-attachments)', 'attachments'},
            }, {prefix = '<localleader>'})
          end,
          pattern = 'himalaya' -- TODO is this the filetype?
        },
      },
    })
  end,
}
