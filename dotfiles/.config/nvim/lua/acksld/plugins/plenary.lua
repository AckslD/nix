return {
  'nvim-lua/plenary.nvim',
  cmd = 'PlenaryBustedDirectory',
  wkeys = {{
    p = {
        name = "+plugin",
        t = {"<Plug>PlenaryTestFile", "test file"},
    },
  }, {prefix = '<leader>'}},
}
