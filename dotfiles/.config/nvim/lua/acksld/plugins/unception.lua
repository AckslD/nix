return {
  "samjwill/nvim-unception",
  config = function()
    R("funcs.autocmds").create_grouped_autocmds({
      MyUnceptionFtermClose = {
        User = {
          pattern = "UnceptionEditRequestReceived",
          callback = function()
            pcall(vim.cmd.FTermCloseAll)
          end,
        },
      },
    })
  end
}
