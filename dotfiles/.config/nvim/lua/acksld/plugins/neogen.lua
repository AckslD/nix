return {
  "danymat/neogen",
  lazy = true,
  module = 'neogen',
  wkeys = {{
    -- NOTE jump commands defined in luasnip config
    l = {
      D = {'<Cmd>lua require("neogen").generate()<CR>', 'docs'},
    },
  }, {prefix = '<leader>'}},
  opts = {
    enabled = true,
  },
  config = 'require("plugin_settings.neogen").config()',
}
