return {
  'simnalamburt/vim-mundo',
  cmd = 'MundoToggle',
  wkeys = {{
    U = {':MundoToggle<CR>', 'mundo toggle'},
  }, {prefix = '<leader>'}},
}
