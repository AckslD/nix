return {
  -- 'https://git.sr.ht/~whynothugo/lsp_lines.nvim',
  'https://git.sr.ht/~acksld/lsp_lines.nvim',
  branch = 'config-options',
  wkeys = {{
    l = {
      V = {'<Cmd>lua vim.diagnostic.toggle_lines()<CR>', 'toggle lsp lines'},
    },
  }, {prefix = '<leader>'}},
  config = function()
    local format = R('funcs.diagnostics').format

    require('lsp_lines').setup()

    local enable_lines = function()
      vim.diagnostic.config({
        virtual_text = {
          source = true,
          format = format,
          severity = {
            max = vim.diagnostic.severity.WARN,
          },
        },
        virtual_lines = {
          source = true,
          format = format,
          severity = vim.diagnostic.severity.ERROR,
        },
      })
    end

    local disable_lines = function()
      vim.diagnostic.config({
        virtual_text = {
          source = true,
          format = format,
        },
        virtual_lines = false,
      })
    end

    local lines_enabled = false

    vim.diagnostic.toggle_lines = function()
      if lines_enabled then
        disable_lines()
      else
        enable_lines()
      end
      lines_enabled = not lines_enabled
    end

    disable_lines()
  end,
}
