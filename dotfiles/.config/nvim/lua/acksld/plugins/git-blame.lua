return {
  'f-person/git-blame.nvim',
  cmd = 'GitBlameToggle',
  wkeys = {{
    g = {
      name = '+git',
      b = {':GitBlameToggle<CR>', 'blame'},
    },
  }, {prefix = '<leader>'}},
  config = function()
    vim.g.gitblame_enabled = 0
    vim.g.gitblame_highlight_group = "Comment"
  end
}
