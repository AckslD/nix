return {
  'CKolkey/ts-node-action',
  lazy = true,
  opts = {
    python = {
      dictionary = {
        {function(node)
          return 'foo'
        end, 'Dict to function'},
      },
    },
  },
  -- config = true,
  wkeys = {{
    k = {function() require('ts-node-action').node_action() end, 'ts node action'},
  }, {prefix = '<leader>'}},
}
