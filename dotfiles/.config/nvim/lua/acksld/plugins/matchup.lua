return {
  'andymass/vim-matchup',
  -- doesn't work for some reason
  -- keys = '%',
  -- event = 'VeryLazy',
  init = function()
    vim.g.matchup_surround_enabled = 1
    vim.g.matchup_matchparen_offscreen = {method = 'popup', highlight = 'TreeSitterContext'}
  end,
  wkeys = {{
    b = {
      m = {'<Cmd>MatchupWhereAmI?<CR>', 'where am I?'},
      M = {'<Cmd>MatchupWhereAmI??<CR>', 'where am I??'},
    },
  }, {prefix = '<leader>'}},
  config = function()
    IfHas('nvim-treesitter.configs', function(ts_config)
      ts_config.setup({
        matchup = {
          enable = true,
        },
      })
    end)
    -- use I and A instead of 1i and 1a
    -- vim.api.nvim_exec([[
    --   function! s:matchup_convenience_maps()
    --     xnoremap <sid>(std-I) I
    --     xnoremap <sid>(std-A) A
    --     xmap <expr> I mode()=='<c-v>'?'<sid>(std-I)':(v:count?'':'1').'i'
    --     xmap <expr> A mode()=='<c-v>'?'<sid>(std-A)':(v:count?'':'1').'a'
    --     for l:v in ['', 'v', 'V', '<c-v>']
    --       execute 'omap <expr>' l:v.'I%' "(v:count?'':'1').'".l:v."i%'"
    --       execute 'omap <expr>' l:v.'A%' "(v:count?'':'1').'".l:v."a%'"
    --     endfor
    --   endfunction
    --   call s:matchup_convenience_maps()
    -- ]], false)
  end,
}
