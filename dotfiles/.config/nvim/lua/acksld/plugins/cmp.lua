return {
  'hrsh7th/nvim-cmp',
  event = 'InsertEnter',
  dependencies = {
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-nvim-lua',
    'saadparwaiz1/cmp_luasnip',
    'onsails/lspkind-nvim',
    'cmp-git',
    'LuaSnip',

  },
  config = function()
    local cmp = require('cmp')
    cmp.setup {
      mapping = cmp.mapping.preset.insert({
        ['<CR>'] = cmp.config.disable,
        -- TODO
        -- ['<C-d>'] = function(fallback)
        --   if cmp.visible() then
        --     cmp.mapping.scroll_docs(-4)
        --   else
        --     fallback()
        --   end
        -- end,
        ['<C-n>'] = function(fallback)
          if cmp.visible() then
            cmp.select_next_item()
          else
            fallback()
          end
        end,
        ['<C-p>'] = function(fallback)
          if cmp.visible() then
            cmp.select_prev_item()
          else
            fallback()
          end
        end,
        ['<C-u>'] = cmp.mapping.scroll_docs(4),
        ['<C-e>'] = cmp.mapping.close(),
        ['<C-k>'] = function(fallback)
          if cmp.visible() then
            cmp.mapping.confirm({ select = true })(fallback)
          else
            cmp.mapping.complete()(fallback)
          end
        end
      }),
      sources = {
        { name = "nvim_lua"},
        { name = "nvim_lsp"},
        -- { name = "path", max_item_count = 3 },
        { name = "luasnip" },
        -- { name = "buffer", keyword_length = 3},
        { name = "git" },
        -- { name = "neorg" },
      },
      snippet = {
        expand = function(args)
          require("luasnip").lsp_expand(args.body)
        end,
      },
      formatting = {
        format = require('lspkind').cmp_format {
          with_text = true,
          menu = {
            nvim_lua = "[Lua]",
            nvim_lsp = "[LSP]",
            -- path = "[Path]",
            luasnip = "[LuaSnip]",
            -- buffer = "[Buffer]",
            cmp_git = "[Git]",
          },
        },
      },
      completion = {
        autocomplete = false,
      },
      experimental = {
        ghost_text = {
          hl_group = 'Nontext',
        },
      },
    }

  -- -- Use buffer source for `/`.
  -- cmp.setup.cmdline('/', {
  --     sources = {
  --         { name = 'buffer' }
  --     }
  -- })

  -- -- Use cmdline & path source for ':'.
  -- cmp.setup.cmdline(':', {
  --     sources = cmp.config.sources({
  --         { name = 'path' }
  --     }, {
  --         { name = 'cmdline' }
  --     })
  -- })
  end,
}
