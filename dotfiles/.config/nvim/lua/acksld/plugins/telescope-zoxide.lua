return {
  'jvgrootveld/telescope-zoxide',
  lazy = true,
  config = function()
    require("telescope").setup({
      extensions = {
        zoxide = {
          mappings = {
            default = {
              action = vim.schedule_wrap(function(selection)
                vim.cmd("tcd " .. selection.path)
              end),
              after_action = function(selection) end,
            },
          },
        },
      },
    })
  end,
}
