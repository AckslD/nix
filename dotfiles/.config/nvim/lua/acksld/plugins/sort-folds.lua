return {
  'obreitwi/vim-sort-folds',
  keys = {
    {'<leader>fs', mode = 'x'},
  },
  wkeys = {{
    f = {
      name = '+folds',
      s = {'<Plug>SortFolds', 'sort'},
    },
  }, {prefix = '<leader>', mode='v', noremap=false}},
  init = function()
    vim.g.sort_folds_ignore_case = true
  end,
}
