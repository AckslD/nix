return {
  'lewis6991/gitsigns.nvim',
  event = 'VeryLazy',
  dependencies = {'plenary.nvim'},
  wkeys = {{
    h = {
      name = '+hunk',
      s = {'<Cmd>lua require("gitsigns").stage_hunk()<CR>', 'stage'},
      u = {'<Cmd>lua require("gitsigns").undo_stage_hunk()<CR>', 'unstage'},
      r = {'<Cmd>lua require("gitsigns").reset_hunk()<CR>', 'reset'},
      R = {'<Cmd>lua require("gitsigns").reset_buffer()<CR>', 'reset buffer'},
      p = {'<Cmd>lua require("gitsigns").preview_hunk()<CR>', 'preview'},
      b = {'<Cmd>lua require("gitsigns").blame_line()<CR>', 'blame'},
      t = {'<Cmd>lua require("gitsigns").toggle_signs()<CR>', 'toggle signs'},
    }
  }, {prefix = '<leader>'}},
  config = function()
    vim.cmd('highlight link GitSignsAdd Title')
    vim.cmd('highlight link GitSignsDelete WarningMsg')
    vim.cmd('highlight link GitSignsChange ModeMsg')

    require('gitsigns').setup {
      signs = {
        add          = {hl = 'GitSignsAdd'   , text = '│'},
        change       = {hl = 'GitSignsChange', text = '│'},
        delete       = {hl = 'GitSignsDelete', text = '_'},
        topdelete    = {hl = 'GitSignsDelete', text = '‾'},
        changedelete = {hl = 'GitSignsChange', text = '~'},
      },
      on_attach = function(bufnr)
        local gs = package.loaded.gitsigns
        local function map(mode, l, r, opts)
          opts = opts or {}
          opts.buffer = bufnr
          vim.keymap.set(mode, l, r, opts)
        end

        -- Navigation
        map('n', ']c', function()
          if vim.wo.diff then return ']c' end
          vim.schedule(function() gs.next_hunk() end)
          return '<Ignore>'
        end, {expr=true})

        map('n', '[c', function()
          if vim.wo.diff then return '[c' end
          vim.schedule(function() gs.prev_hunk() end)
          return '<Ignore>'
        end, {expr=true})

        -- Text object
        map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
      end,
    }
  end,
}
