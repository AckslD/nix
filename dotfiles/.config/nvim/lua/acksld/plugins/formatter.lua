local spec = function(filetype, name)
  return function()
    return require(string.format('formatter.filetypes.%s', filetype))[name]()
  end
end

return {
  'mhartington/formatter.nvim',
  cmd = 'Format',
  init = function()
    WK({
      l = {
        f = { '<Cmd>Format<CR>', 'format' },
        F = { '<Cmd>FormatWrite<CR>', 'format write' },
      },
    }, { prefix = '<leader>' })
    WK({
      l = {
        f = { '<Cmd>Format<CR>', 'format' },
      },
    }, { mode = 'x', prefix = '<leader>' })
  end,
  opts = {
    logging = true,
    log_level = vim.log.levels.WARN,
    filetype = {
      lua = { spec('lua', 'stylua') },
      json = { spec('json', 'jq') },
      python = {
        function()
          return {
            exe = 'autopep8',
            args = { '-', '--experimental' },
            stdin = 1,
          }
        end,
        function()
          return {
            exe = 'isort',
            args = { '-', '-d', '--sp', '~/.isort.cfg' },
            stdin = 1,
          }
        end,
        -- spec('python', 'autopep8'),
        -- spec('python', 'isort'),
      },
      ['*'] = { spec('any', 'remove_trailing_whitespace') },
    },
  },
}
