return {
  'pwntester/octo.nvim',
  cmd = 'Octo',
  wkeys = {{
    g = {
      o = {':Octo<CR>', 'octo'},
    },
  }, {prefix = '<leader>'}},
  config = true,
}
