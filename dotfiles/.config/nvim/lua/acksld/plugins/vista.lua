return {
  'liuchengxu/vista.vim',
  cmd = 'Vista',
  wkeys = {{
    v = {'<Cmd>Vista!!<CR>', 'toggle vista'},
  }, {prefix = '<leader>'}},
}
