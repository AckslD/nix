return {
  'filipdutescu/renamer.nvim',
  lazy = true,
  {{
    r = {
      name = "+refactor",
      n = {'<Cmd>lua require("renamer").rename()<CR>', 'rename (lsp)'},
    },
  }, {prefix = '<leader>'}},
  config = true,
}
