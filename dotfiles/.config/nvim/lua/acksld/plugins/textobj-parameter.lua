return {
  'sgur/vim-textobj-parameter',
  keys = {
    {'a,', mode = 'o'},
    {'i,', mode = 'o'},
    {'a,', mode = 'x'},
    {'i,', mode = 'x'},
  },
  dependencies = {'vim-textobj-user'},
}
