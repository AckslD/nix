return {
  'jbyuki/venn.nvim',
  cmd = 'VBox',
  setup = 'require("plugin_settings.venn").setup()',
  wkeys = {{
    u = {
      name = '+boxes',
      v = {':VBox<CR>', 'draw box'},
      d = {':VBoxD<CR>', 'draw box (double)'},
      h = {':VBoxH<CR>', 'draw box (thick)'},
      c = {
        name = '+crossing',
        v = {':VBoxO<CR>', 'draw (crossing) box'},
        d = {':VBoxDO<CR>', 'draw (crossing) box (double)'},
        h = {':VBoxHO<CR>', 'draw (crossing) box (thick)'},
      },
    },
  }, {prefix = '<leader>', mode = 'v'}},
}
