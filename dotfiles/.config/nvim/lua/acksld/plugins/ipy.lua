return {
  'bfredl/nvim-ipy',
  cmd = {'IPython'},
  init = function()
    vim.g.nvim_ipy_perform_mappings = 0
    vim.g.ipy_celldef = '^##'

    R("funcs.commands").define_commands({
      RunQTConsole = {command = function()
        vim.fn.jobstart('jupyter qtconsole --JupyterWidget.include_other_output=True')
        vim.defer_fn(function()
          vim.cmd.IPython('--existing', '--no-window')
        end, 1000)
      end, buffer = 0},
    })

    R('funcs.autocmds').create_grouped_autocmds({
      MyIPy = {
        FileType = {
          pattern = 'python',
          callback = function()
            WK({
              n = {
                '+ipython',
                o = {function() vim.cmd.RunQTConsole() end, 'start qt and connect'},
                c = {function() vim.cmd.IPython('--existing', '--no-window') end, 'connect to existing'},
                n = {'<Plug>(IPy-Run)', 'run line'},
                r = {'<Plug>(IPy-RunCell)', 'run cell'},
                a = {'<Plug>(IPy-RunAll)', 'run all'},
                t = {'<Plug>(IPy-RunOp)', 'run op'},
                k = {'<Plug>(IPy-WordObjInfo)', 'info cursor'},
                x = {'<Plug>(IPy-Interrupt)', 'interrupt'},
                q = {'<Plug>(IPy-Terminate)', 'terminate'},
              },
            }, {prefix = '<localleader>', silent = true, buffer = 0})
            WK({
              n = {
                '+ipython',
                n = {'<Plug>(IPy-Run)', 'run visual'},
              },
            }, {prefix = '<localleader>', mode = 'x', silent = true, buffer = 0})
          end,
        },
      },
    })
  end,
}
