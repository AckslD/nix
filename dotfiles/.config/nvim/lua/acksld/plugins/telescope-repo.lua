local fd_opts = {
  '--exclude', '.cache',
  '--exclude', 'tmp',
  '--exclude', '.config',
}

return {
  'cljoly/telescope-repo.nvim',
  lazy = true,
  wkeys = {{
    f = {
      p = {string.format('<Cmd>lua require("telescope").extensions.repo.list({fd_opts=%s})<CR>', vim.inspect(fd_opts)), 'projects'},
      P = {'<Cmd>lua require("telescope").extensions.repo.cached_list()<CR>', 'projects (cached)'},
    },
  }, {prefix = '<leader>'}},
}
