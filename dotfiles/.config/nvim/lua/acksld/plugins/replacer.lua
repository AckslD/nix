return {
  'gabrielpoca/replacer.nvim',
  lazy = true,
  wkeys = {{
    q = {
        name = '+quickfix',
        r = {'<Cmd>lua require("replacer").run()<CR>', 'replacer'},
    }
  }, {prefix = '<leader>'}},
}
