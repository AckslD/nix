return {
  'AckslD/Vim-Jinja2-Syntax',
  branch = 'only-syntax',
  ft = 'jinja',
}
