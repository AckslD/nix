return {
  'kevinhwang91/nvim-bqf',
  ft = 'qf',
  opts = {
    func_map = {
      vsplit = '<C-S>',
    },
  },
}
