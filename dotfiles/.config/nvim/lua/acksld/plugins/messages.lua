return {
  'AckslD/messages.nvim',
  cmd = 'Messages',
  init = function()
    Msg = function(...)
      require('messages.api').capture_thing(...)
    end
  end,
  config = true,
}
