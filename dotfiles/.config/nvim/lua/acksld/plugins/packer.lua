return {
  'wbthomason/packer.nvim',
  cmd = {
    'PackerLoad',
    'PackerStatus',
    'PackerInstall',
    'PackerSync',
    'PackerUpdate',
    'PackerClean',
    'PackerCompile',
    'PackerProfile',
  },
  wkeys = {{
    p = {
      name = "plugin",
      d = {"<Cmd>PackerClean<CR>", "packer clean"},
      -- p = {"<Cmd>PackerProfile<CR>", "packer profile"},
      i = {"<Cmd>PackerInstall<CR>", "packer install"},
      l = {"<Cmd>PackerStatus<CR>", "packer status"},
      s = {"<Cmd>PackerSync --preview<CR>", "packer sync"},
      S = {"<Cmd>PackerSync<CR>", "packer sync (no preview"},
      u = {"<Cmd>PackerUpdate --preview<CR>", "packer update"},
      U = {"<Cmd>PackerUpdate<CR>", "packer update (no preview)"},
    }
  }, {prefix = '<leader>', silent = false}},
}
