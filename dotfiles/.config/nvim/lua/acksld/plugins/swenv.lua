return {
  'AckslD/swenv.nvim',
  lazy = true,
  wkeys = {{
    p = {
      -- not really plugin related but hey...
      y = {function() require('swenv.api').pick_venv() end, 'pick venv'},
    },
  }, {prefix = '<leader>', silent = false}},
}
