return {
  'hoob3rt/lualine.nvim',
  dependencies = {
    -- 'nvim-gps',
    'nvim-web-devicons',
    'swenv.nvim',
  },
  config = function()
    local brj = R('funcs.bracket_jump')
    local has_gps, gps = pcall(require, 'nvim-gps')
    local gruvbox = require'lualine.themes.gruvbox'

    local lualine_c_focus = {}
    if has_gps then
      table.insert(lualine_c_focus, #lualine_c_focus + 1, {
        gps.get_location,
        cond = gps.is_available,
      })
    end
    for _, mode in ipairs({
      'normal',
      'insert',
      'visual',
      'replace',
      'command',
      'inactive',
    }) do
      gruvbox[mode].c.fg = '#a89984'
      gruvbox[mode].c.bg = 'none'
    end
    vim.opt.showmode = false

    require('lualine').setup({
      options = {
        -- theme = gruvbox,
        globalstatus = true,
      },
      sections = {
        lualine_a = {'mode'},
        lualine_b = {
          'branch',
          'swenv',
          {
            'diagnostics',
            sources = {'nvim_diagnostic'},
            symbols = {error = '', warn = '', info = '', hint = ''}
          },
        },
        lualine_c = lualine_c_focus,
        lualine_x = {
          function() return ']' .. brj.get() end,
          'encoding',
          'fileformat',
          {'filetype', colored = true},
          'gfold',
        },
        lualine_y = {'progress'},
        lualine_z = {'location'}
      },
      inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = {{
          'filename',
          path = 2, -- absolute
        }},
        lualine_x = {'location'},
        lualine_y = {},
        lualine_z = {}
      },
      -- extensions = {'quickfix'},
    })
  end,
}
