return {
  'dccsillag/magma-nvim',
  -- cmd = 'MagmaInit',
  config = function()
    WK({
      n = {
        name = '+magma',
        e = {'<Cmd>MagmaEvaluateOperator<CR>', 'evaluate operator'},
      },
    }, {expr = true, prefix = '<localleader>'})
    WK({
      n = {
        name = '+magma',
        n = {'<Cmd>MagmaEvaluateLine<CR>', 'evaluate line'},
        c = {'<Cmd>MagmaReevaluateCell<CR>', 're-evaluate cell'},
        d = {'<Cmd>MagmaDelete<CR>', 'delete'},
        o = {'<Cmd>MagmaShowOutput<CR>', 'show output'},
      },
    }, {prefix = '<localleader>'})
    WK({
      n = {
        name = '+magma',
        e = {':<C-u>MagmaEvaluateVisual<CR>', 'evaluate visual'},
      },
    }, {mode = 'x', prefix = '<localleader>'})

    vim.g.magma_automatically_open_output = false
    vim.g.magma_image_provider = "kitty"
  end,
}
