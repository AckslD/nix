return {
  'monaqa/dial.nvim',
  keys = {
    '<C-a>',
    '<C-x>',
    {'<C-a>', mode = 'v'},
    {'<C-x>', mode = 'v'},
    {'g<C-a>', mode = 'v'},
    {'g<C-x>', mode = 'v'},
  },
  config = function()
    local augend = require("dial.augend")
    require("dial.config").augends:register_group{
      default = {
        augend.integer.alias.decimal,
        augend.integer.alias.hex,
        augend.date.alias["%Y/%m/%d"],
        augend.constant.alias.bool,
        augend.constant.new({elements = {'True', 'False'}}),
        augend.constant.new({elements = {'or', 'and'}}),
      },
    }
    -- local dial = require("dial")
    -- dial.augends["custom#boolean"] = dial.common.enum_cyclic{
    --     name = "boolean",
    --     strlist = {"true", "false"},
    -- }
    -- dial.augends["custom#Boolean"] = dial.common.enum_cyclic{
    --     name = "boolean",
    --     strlist = {"True", "False"},
    -- }
    -- table.insert(dial.config.searchlist.normal, "custom#boolean")
    -- table.insert(dial.config.searchlist.normal, "custom#Boolean")
    local opts = {silent = true, noremap=true}
    vim.api.nvim_set_keymap('n', '<C-a>', require('dial.map').inc_normal(), opts)
    vim.api.nvim_set_keymap('n', '<C-x>', require('dial.map').dec_normal(), opts)
    vim.api.nvim_set_keymap('v', '<C-a>', require('dial.map').inc_visual(), opts)
    vim.api.nvim_set_keymap('v', '<C-x>', require('dial.map').dec_visual(), opts)
    vim.api.nvim_set_keymap('v', 'g<C-a>', require('dial.map').inc_gvisual(), opts)
    vim.api.nvim_set_keymap('v', 'g<C-x>', require('dial.map').dec_gvisual(), opts)
  end,
}
