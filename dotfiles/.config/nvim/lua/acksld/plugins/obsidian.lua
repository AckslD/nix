local dir = vim.fn.expand('~/todo/notes')

return {
  "epwalsh/obsidian.nvim",
  version = '*',
  lazy = true,
  event = {
    string.format("BufReadPre %s/**.md", dir),
    string.format("BufNewFile %s/**.md", dir),
  },
  cmd = {
    'ObsidianNew',
    'ObsidianOpen',
    'ObsidianToday',
    'ObsidianYesterday',
    'ObsidianTomorrow',
    'ObsidianQuickSwitch',
  },
  wkeys = {{
    a = {
      name = '+obsidian',
      n = {'<Cmd>ObsidianNew<CR>', 'new'},
      o = {'<Cmd>ObsidianOpen<CR>', 'open'},
      t = {'<Cmd>ObsidianToday<CR>', 'today'},
      T = {'<Cmd>ObsidianTomorrow<CR>', 'tomorrow'},
      y = {'<Cmd>ObsidianYesterday<CR>', 'yesterday'},
      f = {'<Cmd>ObsidianQuickSwitch<CR>', 'quick switch'},
    },
  }, {prefix = '<leader>'}},
  dependencies = {
    "nvim-lua/plenary.nvim",
  },
  opts = {
    dir = dir,
    daily_notes = {
      folder = 'dailies',
      date_format = "%Y_%m_%d"
    },
    completion = {
      nvim_cmp = true,
      min_chars = 0,
    },
    note_id_func = function(title)
      local base = os.date('%Y_%m_%d')
      if title == nil then
        title = os.date('%H%M%S')
      else
        title = title:gsub(" ", "-"):gsub("[^A-Za-z0-9-]", ""):lower()
      end
      return string.format('%s_%s', base, title)
    end,
  },
}
