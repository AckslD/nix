return {
  -- 'nvim-telescope/telescope.nvim',
  'AckslD/telescope.nvim',
  branch = 'on-no-result',
  dependencies = {
    'popup.nvim',
    'plenary.nvim',
    'telescope-symbols.nvim',
    'telescope-file-browser.nvim',
  },
  cmd = 'Telescope',
  keys = {
    {'<C-p>', '<Cmd>Telescope find_files find_command=fd,--hidden,--type,f,--exclude,.git<CR>'},
    {'<C-f>', '<Cmd>Telescope live_grep<CR>'},
  },
  wkeys = {{
    f = {
      name = '+find',
      f = {'<Cmd>lua require("telescope.builtin").builtin({include_extensions = true})<CR>', 'telescope'},
      -- l = {
      --   name = '+lsp',
      --   s = {'<Cmd>Telescope lsp_document_symbols<CR>', 'document symbols'},
      --   S = {'<Cmd>Telescope lsp_workspace_symbols<CR>', 'workspace symbols'},
      -- },
      b = {'<Cmd>Telescope buffers<CR>', 'buffers'},
      h = {'<Cmd>Telescope help_tags<CR>', 'help tags'},
      k = {'<Cmd>Telescope keymaps<CR>', 'keymaps'},
      o = {'<Cmd>Telescope oldfiles<CR>', 'old files'},
      s = {'<Cmd>Telescope current_buffer_fuzzy_find<CR>', 'current buffer'},
      e = {'<Cmd>Telescope symbols<CR>', 'symbols'},
      n = {'<Cmd>lua require("telescope").extensions.file_browser.file_browser()<CR>', 'file browser'},
      -- m = {'<Cmd>Telescope bookmarks<CR>', 'marks'},
      c = {
          name = '+commands',
          c = {'<Cmd>Telescope commands<CR>', 'commands'},
          h = {'<Cmd>Telescope command_history<CR>', 'history'},
      },
      q = {'<Cmd>Telescope quickfix<CR>', 'quickfix'},
      g = {
          name = '+git',
          g = {'<Cmd>Telescope git_commits<CR>', 'commits'},
          c = {'<Cmd>Telescope git_bcommits<CR>', 'bcommits'},
          b = {'<Cmd>Telescope git_branches<CR>', 'branches'},
          s = {'<Cmd>Telescope git_status<CR>', 'status'},
      },
    },
    l = {
        name = '+lsp',
        e = {'<Cmd>Telescope diagnostics bufnr=0<CR>', 'lsp errors'},
        -- h = {'<Cmd>Telescope lsp_references<CR>', 'lsp references'},
        w = {'<Cmd>Telescope lsp_dynamic_workspace_symbols<CR>', 'workspace symbols'},
    },
  }, {prefix = '<leader>'}},
  opts = {
    defaults = {
      dynamic_preview_title = true,
      vimgrep_arguments = {
        "rg",
        "--color=never",
        "--no-heading",
        "--with-filename",
        "--line-number",
        "--column",
        "--smart-case",
        "--hidden",
        "-g","!.git/**",
      },
    },
  },
}
