return {
  'iamcco/markdown-preview.nvim',
  build = vim.fn['mkdp#util#install'],
  ft = 'markdown',
}
