return {
  'numToStr/Comment.nvim',
  keys = {
    'gc',
    'gb',
    'gcc',
    'gbc',
    {'gc', mode = 'x'},
    {'gb', mode = 'x'},
  },
  config = true,
}
