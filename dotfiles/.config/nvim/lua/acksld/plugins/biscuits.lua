return {
  'code-biscuits/nvim-biscuits',
  lazy = true,
  wkeys = {{
    l = {
      b = {'<Cmd>lua require("nvim-biscuits").toggle_biscuits()<CR>', 'toggle biscuits'},
    },
  }, {prefix = '<leader>'}},
  opts = {
    default_config = {
      max_length = 12,
      min_distance = 5,
      -- prefix_string = " 📎 ",
    },
  },
}
