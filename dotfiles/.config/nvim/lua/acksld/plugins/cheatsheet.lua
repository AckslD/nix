return {
  'sudormrfbin/cheatsheet.nvim',
  cmd = 'Cheatsheet',
  wkeys = {{
    K = {':Cheatsheet<CR>', 'cheatsheet'},
  }, {prefix = '<leader>'}},
}
