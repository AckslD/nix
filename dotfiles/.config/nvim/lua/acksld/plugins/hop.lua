return {
  'phaazon/hop.nvim',
  cmd = {
    'HopChar2',
    'HopChar1',
    'HopChar2',
    'HopWord',
    'HopLine',
    'HopPattern',
    'HopWord',
  },
  keys = {
    {'<CR>', '<Cmd>HopWord<CR>'},
  },
  wkeys = {{
    C = {"<Cmd>HopChar2<CR>", "hop char2"},
    [vim.g.mapleader or [[\]]] = {
      name = "hop",
      f = {"<Cmd>HopChar1<CR>", "hop char1"},
      c = {"<Cmd>HopChar2<CR>", "hop char2"},
      w = {"<Cmd>HopWord<CR>", "hop word"},
      l = {"<Cmd>HopLine<CR>", "hop line"},
      p = {"<Cmd>HopPattern<CR>", "hop pattern"},
    }
  }, {prefix = '<leader>', silent = false}},
  init = function()
    local unmap_cr = function()
      vim.api.nvim_buf_set_keymap(0, "n", "<CR>", "<CR>", {noremap = true})
    end
    R('funcs.autocmds').create_grouped_autocmds({
      MyHop = {
        CmdwinEnter = {
          callback = unmap_cr,
        },
        FileType = {
          callback = unmap_cr,
          pattern = 'qf',
        },
      },
    })
  end,
  config = true,
}
