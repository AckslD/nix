return {
  'francoiscabrol/ranger.vim',
  cmd = {'Ranger'},
  keys = {
    {'<C-n>', ':Ranger<CR>', silent = true},
  },
  wkeys = {{
    n = {
      name = '+navigation',
      d = {':DiffVifm<CR>', 'diff files'},
      s = {':SplitVifm<CR>', 'new file split'},
      v = {':VsplitVifm<CR>', 'new file v split'},
      t = {':TabVifm<CR>', 'new file tab'},
      f = {'<Cmd>VifmSelectFile<CR>', 'vifm select'},
    },
  }, {prefix = '<leader>'}},
}
