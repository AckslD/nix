return {
  'AckslD/todo.txt-vim',
  branch = 'develop',
  dependencies = {
    'junegunn/fzf',
    -- 'junegunn/fzf.vim',
  },
  -- ft = 'todo',
  config = function()
    vim.g.todo_load_python = true
    vim.g.todo_focus_initial_projects = {'work'}
    R('funcs.autocmds').create_grouped_autocmds({
      MyIPy = {
        FileType = {
          pattern = 'todo',
          callback = function()
            WK({  -- TODO how to handle?
              s = {
                name = '+sort',
                s = 'sort',
                ['@'] = 'sort_by_context',
                ['+'] = 'sort_by_project',
                d = {
                  name = '+date',
                  s = 'by_date',
                  d = 'by_due_date',
                },
              },
              j = 'prioritize_increase',
              k = 'prioritize_decrease',
              a = 'prioritize_add A',
              b = 'prioritize_add B',
              c = 'prioritize_add C',
              d = 'date pick_date',
              x = 'mark_as_done',
              X = 'mark_all_as_done',
              D = 'remove_completed',
              n = {
                name = '+notes',
                o = 'open',
                d = 'pop',
              },
              t = {
                name = '+subtasks',
                to = 'open',
                td = 'pop',
              },
              l = {
                name = '+links',
                lo = 'open',
                ld = 'pop',
              },
              v = {
                name = '+todokeys',
                vd = 'todokeys pop',
              },
              o = 'tasks insert_new("n", 0, 0)',
              O = 'tasks insert_new("n", 1, 0)',
              p = 'tasks insert_new("n", 0, 1)',
              P = 'tasks insert_new("n", 1, 1)',
              m = 'toggle_backlog',
              z = {
                name = '+folding',
                zp = 'toggle_focus_project',
                zc = 'toggle_focus_context',
                zd = 'toggle_focus_due_date',
                zt = 'focus_query_tag',
              },
            }, {prefix = '<localleader>', buffer = 0})


            WK({
              s = {
                name = '+sort',
                s ='sort',
                ['@'] = 'sort_by_context',
                ['+'] = 'sort_by_project',
                d = {
                  name = '+date',
                  s = 'by_date',
                  d = 'by_due_date',
                },
              },
              c = 'prioritize_add C',
              b = 'prioritize_add B',
              a = 'prioritize_add A',
              k = 'prioritize_decrease',
              j = 'prioritize_increase',
              x = 'mark_as_done',
              m = 'toggle_backlog(visualmode())',
            }, {prefix = '<localleader>', mode = 'v', buffer = 0})
          end,
        },
      },
    })
  end,
}
