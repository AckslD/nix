return {
  'rcarriga/nvim-notify',
  wkeys = {{
    f = {
      i = {function() require('telescope').extensions.notify.notify() end, 'notifications'},
    },
  }, {prefix = '<leader>'}},
  config = function()
    require('notify').setup({
        background_colour = '#000000',
        timeout = 3000,
    })
    vim.notify = require('notify')
  end,
  -- opt = true,
}
