return {
  'kana/vim-textobj-syntax',
  keys = {
    {'ay', mode = 'o'},
    {'iy', mode = 'o'},
    {'ay', mode = 'x'},
    {'iy', mode = 'x'},
  },
  dependencies = {'vim-textobj-user'},
}
