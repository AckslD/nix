return {
  'jpalardy/vim-slime',
  keys = {
    {'<C-c><C-c>', mode = 'x'},
    {'<C-c><C-c>', mode = 'n'},
    {'<C-c><C-v>', mode = 'n'},
  },
  init = function()
    vim.g.slime_target = 'neovim'
    vim.g.slime_python_ipython = 1
    vim.g.slime_get_jobid = function()
      return require('FTerm').get_job_id()
    end
  end,
}
