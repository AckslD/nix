return {
  'L3MON4D3/LuaSnip',
  lazy = true,
  build = "make install_jsregexp",
  config = function()
    local function neogen_jumpable()
      return IfHas('neogen', function(neogen)
        return neogen.jumpable()
      end, function()
          return false
        end)
    end

    vim.cmd [[
        inoremap <silent> <C-d> <cmd>lua require('luasnip').expand()<CR>
        inoremap <silent> <C-f> <cmd>lua require('luasnip').jump(-1)<CR>
        snoremap <silent> <C-f> <cmd>lua require('luasnip').jump(-1)<CR>
        snoremap <silent> <C-t> <cmd>lua require('luasnip').jump(1)<CR>
        ]]
    local ls = require('luasnip')

    vim.keymap.set('i', '<C-t>', function()
      if neogen_jumpable() then
        require('neogen').jump_next()
      elseif ls.jumpable() then
        ls.jump(1)
      else
        vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes([[<C-d>]], true, true, true), 'n', true)
      end
    end, {desc = 'luasnip expand or jump'})
    vim.keymap.set({'i', 's'}, '<C-l>', function()
      if ls.choice_active() then
        ls.change_choice(1)
      else
        vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes([[<C-l>]], true, true, true), 'n', true)
      end
    end, {desc = 'luasnip cycle choice'})

    local s = ls.snippet
    local i = ls.insert_node
    local t = ls.text_node
    local f = ls.function_node
    local d = ls.dynamic_node
    local c = ls.choice_node
    local r = ls.restore_node
    local sn = ls.snippet_node
    local isn = ls.indent_snippet_node
    local rep = require('luasnip.extras').rep
    local fmt = require('luasnip.extras.fmt').fmt
    local fmta = require("luasnip.extras.fmt").fmta
    local types = require("luasnip.util.types")
    local events = require("luasnip.util.events")

    vim.api.nvim_command("hi LuasnipChoiceNodePassive cterm=italic")

    ls.config.set_config({
      history = true,
      updateevents = 'TextChanged,TextChangedI',
      delete_check_events = 'TextChanged,TextChangedI',
      region_check_events = 'CursorMoved',
      ext_opts = {
        [types.insertNode] = {
          passive = {
            hl_group = "GruvboxRed"
          }
        },
        [types.choiceNode] = {
          active = {
            virt_text = {{"choiceNode", "GruvboxOrange"}}
          }
        },
        [types.textNode] = {
          passive = {
            hl_group = "GruvboxGreen"
          }
        },
      },
      ext_base_prio = 200,
      ext_prio_increase = 3,
    })

    local repi = function(index, source)
      return d(index, function(arguments)
        return sn(index, i(1, arguments[1]))
      end, {source})
    end

    local function node_with_virtual_text(pos, node, text)
      local nodes
      if node.type == types.textNode then
        node.pos = 2
        nodes = {i(1), node}
      else
        node.pos = 1
        nodes = {node}
      end
      return sn(pos, nodes, {
        callbacks = {
          -- node has pos 1 inside the snippetNode.
          [1] = {
            [events.enter] = function(nd)
              -- node_pos: {line, column}
local node_pos = nd.mark:pos_begin()
              -- reuse luasnips namespace, column doesn't matter, just 0 it.
              nd.virt_text_id = vim.api.nvim_buf_set_extmark(0, ls.session.ns_id, node_pos[1], 0, {
                virt_text = {{text, "GruvboxOrange"}}
              })
            end,
            [events.leave] = function(nd)
              vim.api.nvim_buf_del_extmark(0, ls.session.ns_id, nd.virt_text_id)
            end
          }
        }
      })
    end

    local function nodes_with_virtual_text(nodes, opts)
      if opts == nil then
        opts = {}
      end
      local new_nodes = {}
      for pos, node in ipairs(nodes) do
        if opts.texts[pos] ~= nil then
          node = node_with_virtual_text(pos, node, opts.texts[pos])
        end
        table.insert(new_nodes, node)
      end
      return new_nodes
    end

    local function choice_text_node(pos, choices, opts)
      choices = nodes_with_virtual_text(choices, opts)
      return c(pos, choices, opts)
    end

    local ct = choice_text_node

    ls.add_snippets('all', {
      ls.parser.parse_snippet(
        'copyright',
        [[Copyright (C) Axel Dahlberg ]] .. os.date("%Y")
      ),
      s('test', ct(1, {
        t('a'),
        t('b'),
      }, {
          texts = {
            'choice a',
            'choice b',
          }
        })),
    }, {key = 'my_all_snippets'})

    ls.add_snippets('lua', {
      ls.parser.parse_snippet(
        'req',
        [[local ${1} = require('$1')$0]]
      ),
      s('um', fmt([[
        local M = {{}}

        M.setup = function()
          {setup}
        end

        M.config = function()
          {config}
        end

        return M
      ]], {
          setup = i(1),
          config = i(2),
        })),
      s('u', c(1, {
        fmt([[
          use {{ -- {name} {{{{{{
              '{plugin}',
          }} -- }}}}}}]], {
            plugin = r(1, "plugin"),
            name = d(2, function(plugin)
              plugin = plugin[1][1]
              local name = plugin:gsub('.*/', ''):gsub('nvim%-', '')
              return sn(nil, t(name))
            end, {1}),
          }),
        fmt([[
          use {{ -- {name} {{{{{{
              '{plugin}',
              setup = 'require("plugin_settings.{module}").setup()',
              config = 'require("plugin_settings.{module}").config()',
          }} -- }}}}}}]], {
            plugin = r(1, "plugin"),
            name = d(2, function(plugin)
              plugin = plugin[1][1]
              local name = plugin:gsub('.*/', ''):gsub('nvim%-', '')
              return sn(nil, t(name))
            end, {1}),
            module = f(function(name)
              name = name[1][1]
              return name:gsub([[%.lua]], '')
            end, {2}),
          }),
      }, {restore_cursor = true}), {
          stored = {plugin = i(1)}
        })
    }, {key = 'my_lua_snippets'})

    ls.add_snippets('python', {
      s('t', fmt([[
        @pytest.mark.parametrize('{arguments}', [
            ({params}),
        ])
        def test_{case}({args}):
            {body}]], {
          arguments = i(2, 'arguments'),
          params = d(3, function(arguments)
            arguments = vim.split(arguments[1][1], ',%s*')
            if #arguments == 1 then
              return sn(3, fmt('{}', i(1, arguments[1])))
            else
              local ps = {}
              local is = {}
              for idx, arg in ipairs(arguments) do
                table.insert(ps, '{}')
                table.insert(is, i(idx, arg))
              end
              return sn(3, fmt(table.concat(ps, ', '), is))
            end
          end, {2}),
          case = i(1, 'case'),
          args = d(4, function(arguments)
            arguments = vim.fn.split(arguments[1][1], [[,\s*\zs]])
            local is = {}
            for idx, arg in ipairs(arguments) do
              table.insert(is, i(idx, arg))
            end
            return sn(4, fmt(('{}'):rep(#arguments), is))
          end, {2}),
          body = i(0),
        })),
      s('d', fmt([[
          def {func}({args}){ret}:
              {doc}{body}
      ]], {
          func = i(1),
          args = i(2),
          ret = c(3, {
            t(''),
            sn(nil, {
              t(' -> '),
              i(1),
            }),
          }),
          doc = isn(4, {ct(1, {
            t(''),
            sn(1, fmt([[
              """{desc}"""

              ]], {desc = i(1)})),
            sn(2, fmt([[
              """{desc}

              Args:
              {args}

              Returns:
              {returns}
              """

              ]], {
                desc = i(1),
                args = i(2),  -- TODO should read from the args in the function
                returns = i(3),
              })),
          }, {
              texts = {
                "(no docstring)",
                "(single line docstring)",
                "(full docstring)",
              }
            })}, "$PARENT_INDENT\t"),
          body = i(0),
        })),
      ls.parser.parse_snippet(
        'd"', [[def ${1:function}(${2:arguments}) -> ${3:return}:
"""${4:description}

Args:
${5:$2}

Returns:
${6:$3}
"""
$0]]
      ),
      ls.parser.parse_snippet(
        'c',
        [[class ${1:class}(${2:inherit}):
$0]]
      ),
      ls.parser.parse_snippet(
        'i',
        [[if ${1:condition}:
$0]]
      ),
      ls.parser.parse_snippet(
        'f',
        [[for ${1:i} in ${2:iter}:
$0]]
      ),
      ls.parser.parse_snippet(
        'r',
        [[raise ${1:Value}Error('${2:msg}')]]
      ),
      ls.parser.parse_snippet(
        'if',
        [[from ${1:pkg} import ${2:module}]]
      ),
      ls.parser.parse_snippet(
        'ifa',
        [[from ${1:pkg} import ${2:module} as ${3:alias}]]
      ),
      ls.parser.parse_snippet(
        'im',
        [[import ${1:pkg}]]
      ),
      ls.parser.parse_snippet(
        'ima',
        [[import ${1:pkg} as ${2:alias]]
      ),
      ls.parser.parse_snippet(
        '@p',
        [[@pytest.mark.parametrize('${1:arguments}', [
$0
])]]
      ),
      ls.parser.parse_snippet(
        '@d', [[@dataclass
class ${1:class}:
$0]]
      ),
      ls.parser.parse_snippet(
        '"', [["""$0
"""]]
      ),
      ls.parser.parse_snippet(
        'doc', [["""${1:description}

Args:
${2:args}

Returns:
${3:returns}
"""
@0]]
      ),
      ls.parser.parse_snippet(
        'setup', [[from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
name='${1:name}',
version='0.0.0',
description='${2:description}',
long_description=long_description,
long_description_content_type='text/markdown',
url='${3:https://github.com/AckslD/${1}}',
author='${4:Axel Dahlberg}',
author_email='${5:git@valleymnt.com}',
package_dir={'': 'src'},
packages=find_packages(where='src'),
python_requires='>=${6:3.9}, <4',
install_requires=[${7}],
)]]
      ),
      s('co', fmt([[
        .. code-block:: python

            {code}

      ]], {
          code = i(1, 'code'),
        })),
      s('ex', fmt([[
        Examples:

            .. code-block:: python

                {code}

      ]], {
        code = i(1, 'code'),
      })),
      s('tr', fmt([[
        import traceback
        traceback.print_exc()
      ]], {})
      ),
      s('tt', fmt([[
        from acksld import TaskTimer
        t = TaskTimer()
      ]], {})
      ),
      s('dpy', fmt([[
        import debugpy
        debugpy.listen({port})
        debugpy.wait_for_client()
        debugpy.breakpoint()
      ]], {
        port = i(1, '38000'),
      })),
      s('pp', fmt([[
        from pprint import pprint
        pprint({thing}, width=200)
      ]], {
        thing = i(1, 'thing'),
      })),
      s('ma', fmt([[
        if __name__ == '__main__':
            main()
      ]], {
      })),
    }, {key = 'my_python_snippets'})

    ls.add_snippets('tex', {
      s('d', fmta([[
          \documentclass{article}
          \usepackage[utf8]{inputenc}
          \begin{document}
            <>
          \end{document}
      ]], {
          i(1),
        })),
      s('f', fmta([[
          \begin{figure}
            \includegraphics[width=<>\linewidth]{<>}
            \caption{<>}
            \label{fig:<>}
          \end{figure}
      ]], {
          i(1),
          i(2),
          i(3),
          i(4),
        })),
    }, {key = 'my_tex_snippets'})

    ls.add_snippets('rust', {
      s('t', fmt([[
        #[cfg(test)]
        mod tests {{
            use super::*;

            #[test]
            fn test_{func}() {{
                assert_eq!(1, 1);
            }}
        }}]], {func = i(1)})),
    })
  end,
}
