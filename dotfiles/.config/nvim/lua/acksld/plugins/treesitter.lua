return {
  'nvim-treesitter/nvim-treesitter',
  build = ':TSUpdate',
  event = 'VeryLazy',
  dependencies = {
    'nvim-treesitter-context',
  },
  config = function()
    local parser_config = require("nvim-treesitter.parsers").get_parser_configs()

    -- parser_config.todotxt = {
    --   install_info = {
    --     url = "https://github.com/arnarg/tree-sitter-todotxt",
    --     files = {"src/parser.c"},
    --     branch = "main",
    --   },
    -- }

    -- parser_config.jinja2 = {
    --   install_info = {
    --     url = "https://github.com/theHamsta/tree-sitter-jinja2.git",
    --     -- url = "https://github.com/dbt-labs/tree-sitter-jinja2.git",
    --     files = {"src/parser.c"},
    --     branch = 'master',
    --     -- branch = 'main',
    --   },
    --   -- filetype = 'jinja',
    -- }

    require'nvim-treesitter.configs'.setup({
      ensure_installed = {
        'lua',
        'python',
        'rust',
        'vim',
        'vimdoc',
        'bash',
        'dockerfile',
        'json',
        'latex',
        'yaml',
        'html',
        'c',
        'cpp',
        'markdown',
        'markdown_inline',
        'nix',
      },
      highlight = {
          enable = true,
          -- disable = function()
          --   return vim.bo.filetype == "json" and vim.api.nvim_buf_line_count(0) > 1000
          -- end,
          disable = {'json'},
          additional_vim_regex_highlighting = {'python'},
      },
      incremental_selection = {
        enable = true,
        keymaps = {
          init_selection = 'gni',
          node_incremental = 'gnn',
          scope_incremental = 'gno',
          node_decremental = 'gne',
        },
      },
      indent = {
          enable = true,
          disable = {'python'},
      },
      -- folding = {enable = true},
    })

    -- vim.o.foldmethod = 'expr'
    -- vim.cmd('set foldexpr=nvim_treesitter#foldexpr()')
    -- vim.cmd('set foldexpr=v:lua.vim.treesitter.foldexpr()')
    -- vim.opt.foldexpr = 'line("$") > 10000 ? 0 : v:lua.vim.treesitter.foldexpr()'
    vim.cmd('highlight! link TSDanger Type')
    R('funcs.autocmds').create_grouped_autocmds({
      MyTermNoHighlight = {
        TermOpen = {
          command = 'TSBufDisable highlight',
        },
      },
    })
  end,
}
