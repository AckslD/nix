return {
  'nvim-neorg/neorg',
  dependencies = {'nvim-neorg/neorg-telescope'},
  wkeys = {{
    a = {
      name = '+neorg',
      o = {'<Cmd>edit ~/todo/neorg/index.norg<CR>', 'open index'},
      i = {'<Cmd>edit ~/todo/neorg/inbox.norg<CR>', 'open inbox'},
      c = {'<Cmd>Neorg gtd capture<CR>', 'capture'},
      e = {'<Cmd>Neorg gtd edit<CR>', 'edit'},
      v = {'<Cmd>Neorg gtd views<CR>', 'views'},
      n = {'<Cmd>Neorg news all<CR>', 'news'},
      p = {'<Cmd>Neorg presenter start<CR>', 'start'},
      P = {'<Cmd>Neorg presenter close<CR>', 'close'},
      w = {'<Cmd>Neorg workspaces<CR>', 'workspaces'},
    }
  }, {prefix = '<leader>'}},
  opts = {
    load = {
      ["core.highlights"] = {
        config = {
          highlights = {
            tags = {
              carryover = {
                begin = "+TSConstant",
                name = {
                  word = "+TSConstant",
                },
              },
            },
          },
        },
      },
      ["core.defaults"] = {},
      ["core.norg.concealer"] = {},
      ["core.export"] = {},
      ["core.gtd.base"] = {
        config = {
          workspace = 'home',
        },
      },
      ["core.gtd.ui"] = {},
      ["core.gtd.helpers"] = {},
      ["core.gtd.queries"] = {},
      ["core.norg.completion"] = {
        config = {
          engine = 'nvim-cmp',
        },
      },
      ["core.integrations.nvim-cmp"] = {},
      ["core.presenter"] = {
        config = {
          zen_mode = "zen-mode",
        },
      },
      ["core.export.markdown"] = {
        config = {
          extensions = "all",
        },
      },
      ["core.norg.dirman"] = {
        config = {
          workspaces = {
            home = "~/todo/neorg",
          },
        },
      },
    },
  },
}
