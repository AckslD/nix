return {
  "ThePrimeagen/refactoring.nvim",
  lazy = true,
  init = function()
    WK({
      r = {
        name = '+refactor',
        e = {[[ <Esc><Cmd>lua require('refactoring').refactor('Extract Function')<CR>]], 'extract function'},
        f = {[[ <Esc><Cmd>lua require('refactoring').refactor('Extract Function To File')<CR>]], 'extract to file'},
        v = {[[ <Esc><Cmd>lua require('refactoring').refactor('Extract Variable')<CR>]], 'extract variable'},
        i = {[[ <Esc><Cmd>lua require('refactoring').refactor('Inline Variable')<CR>]], 'inline variable'},
        t = {[[ <Esc><Cmd>lua require('refactoring').select_refactor()<CR>]], 'select refactors'},
      }
    }, {prefix = '<leader>', mode='v'})
    WK({
      r = {
        name = '+refactor',
        b = {[[ <Esc><Cmd>lua require('refactoring').refactor('Extract Block')<CR>]], 'extract block'},
        B = {[[ <Esc><Cmd>lua require('refactoring').refactor('Extract Block To File')<CR>]], 'extract block to file'},
        i = {[[ <Esc><Cmd>lua require('refactoring').refactor('Inline Variable')<CR>]], 'inline variable'},
      }
    }, {prefix = '<leader>', mode='n'})
  end,
  config = true,
}
