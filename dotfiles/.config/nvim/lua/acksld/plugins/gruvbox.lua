return {
  'npxbr/gruvbox.nvim',
  lazy = true,
  dependencies = {"rktjmp/lush.nvim"},
}
