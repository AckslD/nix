return {
  'karb94/neoscroll.nvim',
  keys = {
    '<C-u>',
    '<C-d>',
    '<C-y>',
    '<C-e>',
    'zt',
    'zz',
    'zb',
  },
  config = function()
    if vim.g.neovide then
      return
    end
    require('neoscroll').setup({
      mappings = {
        '<C-u>',
        '<C-d>',
        '<C-y>',
        '<C-e>',
        'zt',
        'zz',
        'zb',
      },
    })

    local t = {}
    t['<C-u>'] = {'scroll', {'-vim.wo.scroll', 'true', '75'}}
    t['<C-d>'] = {'scroll', { 'vim.wo.scroll', 'true', '75'}}
    t['<C-y>'] = {'scroll', {'-0.10', 'false', '30'}}
    t['<C-e>'] = {'scroll', { '0.10', 'false', '30'}}
    t['zt']    = {'zt', {'75'}}
    t['zz']    = {'zz', {'75'}}
    t['zb']    = {'zb', {'75'}}

    require('neoscroll.config').set_mappings(t)
  end,
}
