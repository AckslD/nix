return {
  'AckslD/muren.nvim',
  config = true,
  wkeys = {{
    s = {
      m = {'<Cmd>MurenToggle<CR>', 'muren toggle'},
    },
  }, {prefix = '<leader>'}},
}
