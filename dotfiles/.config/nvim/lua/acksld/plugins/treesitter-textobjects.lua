return {
  'nvim-treesitter/nvim-treesitter-textobjects',
  keys = {
    {'a,', mode = 'o'},
    {'i,', mode = 'o'},
    {'at', mode = 'o'},
    {'it', mode = 'o'},
    {'af', mode = 'o'},
    {'ac', mode = 'o'},
    {'if', mode = 'o'},
    {'ic', mode = 'o'},
    {'a,', mode = 'v'},
    {'i,', mode = 'v'},
    {'at', mode = 'v'},
    {'it', mode = 'v'},
    {'af', mode = 'v'},
    {'ac', mode = 'v'},
    {'if', mode = 'v'},
    {'ic', mode = 'v'},
    '<Leader>Lf',
    '<Leader>Lc',
    '<Leader>L,',
    '<Leader>Hf',
    '<Leader>Hc',
    '<Leader>H,',
    '<Leader>lpf',
    '<Leader>lpc',
    "]m",
    "]M",
    "[m",
    "[M",
    "[]",
  },
  wkeys = {{
    l = {
      p = {
        name = '+peek',
        f = 'function',
        c = 'class',
      },
    },
    H = {
      name = '+swap⬅',
      c = 'class',
      f = 'function',
      [','] = 'parameter',
    },
    L = {
      name = '+swap➡',
      c = 'class',
      f = 'function',
      [','] = 'parameter',
    },
  },
  {prefix = '<leader>'}},
  config = function()
    require "nvim-treesitter.configs".setup({
      textobjects = {
        select = {
          enable = true,
          lookahead = true,
          include_surrounding_whitespace = function(opts)
            local obj, scope = unpack(vim.split(opts.query_string, '.', {plain = true}))
            return scope == 'outer'
          end,
          keymaps = {
            ["at"] = "@annotation.outer",
            ["it"] = "@annotation.inner",
            ["ac"] = "@class.outer",
            ["ic"] = "@class.inner",
            ["af"] = "@function.outer",
            ["if"] = "@function.inner",
            ['i,'] = "@parameter.inner",
            ['a,'] = "@parameter.outer",
          },
          selection_modes = function(opts)
            if vim.bo.filetype == 'python' then
              return {
                ["@class.outer"] = 'V',
                ["@class.inner"] = 'V',
                ["@function.outer"] = 'V',
                ["@function.inner"] = 'V',
              }
            end
          end,
        },
        swap = {
          enable = true,
          swap_next = {
            ["<leader>Lc"] = "@class.inner",
            ["<leader>Lf"] = "@function.inner",
            ["<leader>L,"] = "@parameter.inner",
          },
          swap_previous = {
            ["<leader>Hc"] = "@class.inner",
            ["<leader>Hf"] = "@function.inner",
            ["<leader>H,"] = "@parameter.inner",
          },
        },
        move = {
          enable = true,
          goto_next_start = {
            ["]m"] = "@function.outer",
            ["]]"] = "@class.outer",
          },
          goto_next_end = {
            ["]M"] = "@function.outer",
            ["]["] = "@class.outer",
          },
          goto_previous_start = {
            ["[m"] = "@function.outer",
            ["[["] = "@class.outer",
          },
          goto_previous_end = {
            ["[M"] = "@function.outer",
            ["[]"] = "@class.outer",
          },
        },
        lsp_interop = {
          enable = true,
          border = 'none',
          peek_definition_code = {
            ["<leader>lpf"] = "@function.outer",
            ["<leader>lpc"] = "@class.outer",
          },
        },
      },
    })
  end,
}
