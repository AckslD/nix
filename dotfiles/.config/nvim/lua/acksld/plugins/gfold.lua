return {
  'AckslD/nvim-gfold.lua',
  lazy = true,
  wkeys = {{
    n = {
      g = {function()
        require('gfold').pick_repo()
      end, 'gfold picker'},
      G = {function()
        require('gfold').pick_repo(function(repo)
          return repo.status ~= 'clean'
        end)
      end, 'gfold picker (non-clean)'},
    },
  }, {prefix = '<leader>'}},
  config = function()
    if vim.fn.executable('gfold') == 0 then
      return
    end
    require('gfold').setup({
      cwd = vim.fn.expand('~/dev'),
      no_error = true,
    })
  end,
}
