return {
  'norcalli/nvim-colorizer.lua',
  cmd = 'ColorizerToggle',
  wkeys = {{
    u = {
        name = '+unicode/color',
        c = {'<Cmd>ColorizerToggle<CR>', 'colorizer'},
    },
  }, {prefix = '<leader>'}},
  config = true,
}
