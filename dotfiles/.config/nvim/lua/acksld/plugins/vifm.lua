return {
  'vifm/vifm.vim',
  cmd = {'Vifm', 'DiffVifm', 'SplitVifm', 'VsplitVifm', 'TabVifm'},
  keys = {
    {'<C-n>', ':Vifm<CR>'},
  },
  wkeys = {
    n = {
      name = '+navigation',
      d = {':DiffVifm<CR>', 'diff files'},
      s = {':SplitVifm<CR>', 'new file split'},
      v = {':VsplitVifm<CR>', 'new file v split'},
      t = {':TabVifm<CR>', 'new file tab'},
      f = {'<Cmd>VifmSelectFile<CR>', 'vifm select'},
    },
  },
  setup = 'require("plugin_settings.vifm").setup()',
}
