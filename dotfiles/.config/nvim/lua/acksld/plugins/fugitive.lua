return {
  'tpope/vim-fugitive',
  cmd = {
    'Git',
    'Gdiffsplit',
    'GBrowse',
    'Gclog',
    'Gread',
    'Gwrite',
    'Gedit',
    'GRemove',
    'GDelete',
    'GRename',
    'GMove',
  },
  wkeys = {{
    g = {
      name = '+git',
      -- d = {':Gdiffsplit!<CR>', 'diff fix'},
      -- h = {':diffget LO<CR>', 'diff get local'},
      -- k = {':diffget BE<CR>', 'diff get base'},
      -- l = {':diffget RE<CR>', 'diff get remote'},
      -- g = {':GBrowse<CR>', 'browse'},
      c = {':Gclog!<CR>', 'git log qf'},
    },
  }, {prefix = '<leader>'}},
}
