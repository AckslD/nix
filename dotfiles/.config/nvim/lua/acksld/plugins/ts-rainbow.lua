return {
  'p00f/nvim-ts-rainbow',
  cmd = 'TSRainbowToggle',
  init = function()
    local enabled = true
    local toggle = function()
      if enabled then
        enabled = false
        require'nvim-treesitter.configs'.setup {
          rainbow = {enable = false},
        }
      else
        enabled = true
        require'nvim-treesitter.configs'.setup {
          rainbow = {enable = true},
        }
      end
    end

    R('funcs.commands').define_commands({
      TSRainbowToggle = {
        command = toggle,
        desc = 'toggle ts rainbow',
      },
    })
  end,
  config = function()
    vim.cmd("highlight! link rainbowcol1 GruvboxOrangeBold")
    vim.cmd("highlight! link rainbowcol2 GruvboxPurpleBold")
    vim.cmd("highlight! link rainbowcol3 GruvboxBlueBold")
    vim.cmd("highlight! link rainbowcol4 GruvboxYellowBold")
    vim.cmd("highlight! link rainbowcol5 GruvboxAquaBold")
    vim.cmd("highlight! link rainbowcol6 GruvboxRedBold")
    vim.cmd("highlight! link rainbowcol7 GruvboxGrayBold")
  end,
}
