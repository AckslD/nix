return {
  'folke/trouble.nvim',
  dependencies = {'nvim-web-devicons'},
  cmd = {'Trouble', 'TroubleClose', 'TroubleToggle', 'TroubleRefresh'},
  wkeys = {{
    z = {
      name = '+trouble',
      z = {'<Cmd>TroubleToggle<CR>', 'toggle'},
      w = {'<Cmd>TroubleToggle workspace_diagnostics<CR>', 'workspace'},
      d = {'<Cmd>TroubleToggle document_diagnostics<CR>', 'diagnostics'},
      q = {'<Cmd>TroubleToggle quickfix<CR>', 'quickfix'},
      l = {'<Cmd>TroubleToggle loclist<CR>', 'loclist'},
      r = {'<Cmd>TroubleToggle lsp_references<CR>', 'references'},
    },
  }, {prefix = '<leader>'}},
  config = true,
}
