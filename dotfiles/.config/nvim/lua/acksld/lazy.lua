local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local wrap_wk = function(specs)
  for _, spec in ipairs(specs) do
    if spec.wkeys then
      local current_init = spec.init
      spec.init = function()
        if current_init then
          current_init()
        end
        local wkey_specs = spec.wkeys
        if wkey_specs[2] and wkey_specs[2].prefix then
          wkey_specs = {spec.wkeys}
        end
        IfHas('which-key', function(wk)
          for _, wkey_spec in ipairs(wkey_specs) do
            wk.register(wkey_spec[1], wkey_spec[2])
          end
        end)
      end
    end
  end
  return specs
end

local set = function(things)
  local new = {}
  for _, thing in ipairs(things) do
    new[thing] = true
  end
  return new
end

local plugins_path = vim.fs.normalize('~/.config/nvim/lua/acksld/plugins')

local get_default_modules = function(ignores)
  local modules = {}
  for filepath in vim.fs.dir(plugins_path) do
    local filename = vim.fs.basename(filepath:gsub('%.lua', ''))
    if filename:sub(1, 1) ~= '.' and not ignores[filename] then
      table.insert(modules, filename)
    end
  end
  return modules
end

local handle_bisect = function(ignores)
  local bisect_in_path = plugins_path .. '/.bisect/input.json'
  local bisect_out_path = plugins_path .. '/.bisect/output.json'
  if vim.fn.filereadable(bisect_in_path) ~= 0 then
    local bisect_in = vim.fn.json_decode(vim.fn.readfile(bisect_in_path))
    if bisect_in.query == 'list' then
      local modules = get_default_modules(ignores)
      vim.fn.writefile(vim.fn.split(vim.fn.json_encode(modules), '\n'), bisect_out_path)
    elseif bisect_in.query == 'use_plugins' then
      return bisect_in.plugins
    end
  end
end

local make_plugins = function(ignore)
  local ignores = set(ignore or {})
  local modules = handle_bisect(ignores) or get_default_modules(ignores)
  local specs = {}
  for _, module in ipairs(modules) do
    local spec = R('plugins.'..module)
    table.insert(specs, spec)
  end
  return wrap_wk(specs)
end

local make_only_plugins = function(modules)
  local specs = {}
  for _, module in ipairs(modules) do
    local spec = R('plugins.'..module)
    table.insert(specs, spec)
  end
  return wrap_wk(specs)
end

require("lazy").setup(make_plugins({
  -- NOTE this is filenames, not plugin names
  'anki',
  'bufmax',
  'calltree',
  'capword',
  'dashboard',
  'estream',
  'gps',
  'kommentary',
  'magma',
  'mkdx',
  'nodo',
  'noice',
  'neorg',
  'lua-gf',
  'lsp-notify',
  'packer',
  'ranger',
  'renamer',
  'scrollbar',
  'shade',
  'textobj-parameter',
  'vifm',
  'yati',
  'tmindent',
}), {
-- require("lazy").setup(make_only_plugins({
--   'treesitter',
--   'treesitter-context',
--   'nightfox',
--   'which-key',
-- }), {
  lockfile = vim.fn.expand('~/dev/config/nvim/lazy-lock.json'),
  dev = {
    path = vim.fn.expand('~/dev/vim-plugins'),
    -- patterns = {'AckslD'},
  },
})
