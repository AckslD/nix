local M = {}
local cm = R("funcs.commands")

local function get_dedterm_cmd(settings, precmd)
  return function(opts)
    local args
    if opts ~= nil then
      args = opts.args
    end
    vim.cmd('wa')
    R('funcs.terminal').dedicated(settings.cmd, {
      precmd = precmd,
      args = args,
      post_args = settings.post_args,
    })
    if settings.after then
      settings.after()
    end
  end
end

local function setup_keymap(settings)
    local keymap = {}
    if (settings.run.cmd ~= nil) then
        keymap.r = {get_dedterm_cmd(settings.run, "tabnew"), 'run tab'}
    end
    if (settings.test.cmd ~= nil) then
        keymap.t = {get_dedterm_cmd(settings.test, "tabnew"), 'test tab'}
    end
    IfHas('which-key', function(wk) wk.register(keymap, {prefix = '<localleader>', buffer = 0}) end)
end

local function setup_commands(settings)
    if settings.run.cmd ~= nil then
        cm.define_commands({
            Run = {command = get_dedterm_cmd(settings.run, "tabnew"), nargs = '*', buffer = 0},
            RunSplit = {command = get_dedterm_cmd(settings.run, "split"), nargs = '*', buffer = 0},
            RunVSplit = {command = get_dedterm_cmd(settings.run, "vsplit"), nargs = '*', buffer = 0},
        })
    end
    if settings.test.cmd ~= nil then
        cm.define_commands({
            Test = {command = get_dedterm_cmd(settings.test, "tabnew"), nargs = '*', buffer = 0},
            TestSplit = {command = get_dedterm_cmd(settings.test, "split"), nargs = '*', buffer = 0},
            TestVSplit = {command = get_dedterm_cmd(settings.test, "vsplit"), nargs = '*', buffer = 0},
        })
    end
end

M.setup = function(settings)
    settings = settings or {}
    settings.run = settings.run or {}
    settings.test = settings.test or {}

    setup_keymap(settings)
    setup_commands(settings)
end

return M
