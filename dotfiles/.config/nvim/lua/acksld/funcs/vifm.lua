local M = {}

-- local keep = R('funcs.keep')

local get_vifm_select_cmd = function()
  local buf_name = vim.api.nvim_buf_get_name(0)
  if buf_name ~= '' and not string.match(vim.api.nvim_buf_get_name(0), '^term://') then
    return string.format('vifm --select %s', buf_name)
  else
    return string.format('vifm %s', vim.fn.getcwd())
  end
end

local buf_delete_if_valid = function(buf, opts)
  if vim.api.nvim_buf_is_valid(buf) then
    vim.api.nvim_buf_delete(buf, opts)
  end
end

local temp_term = function(cmd, opts)
  local buf = vim.api.nvim_create_buf(true, false)
  vim.api.nvim_set_current_buf(buf)
  for _, on_event_name in ipairs({
    'on_exit',
    'on_stdout',
    'on_stderr',
  }) do
    local on_event = opts[on_event_name]
    if on_event then
      opts[on_event_name] = function(id, data, event)
        on_event(buf, id, data, event)
      end
    end
  end
  vim.fn.termopen(cmd, opts)
  -- vim.api.nvim_create_autocmd('BufLeave', {
  --   callback = function()
  --     buf_delete_if_valid(buf, {force = true})
  --   end,
  --   buffer = buf,
  -- })
end

-- selects a file(/s) using vifm a does something with them
M.select = function(callback)
    local buf = vim.api.nvim_get_current_buf()
    local pos = vim.api.nvim_win_get_cursor(0)
    local tempfile = vim.fn.tempname()
    local cmd = string.format("%s --choose-file=%s", get_vifm_select_cmd(), tempfile)
    temp_term(cmd, {on_exit = function(term_buf, id, data, event)
      local success, lines = pcall(vim.fn.readfile, tempfile)
      if not success then
        return
      end
      callback({
        original_buf = buf,
        original_pos = pos,
        files = lines
      })
      -- keep.noautocmd(function()
      --   callback({
      --     original_buf = buf,
      --     original_pos = pos,
      --     files = lines
      --   })
      -- end, 'BufLeave')
      buf_delete_if_valid(term_buf, {force = true})
    end})
end

M.select_and_edit = function()
  M.select(function(opts)
    for _, file in ipairs(opts.files) do
      vim.cmd.edit(file)
    end
  end)
end

-- selects a file using vifm a places it in a register (defaults to ")
M.select_to_register = function(reg)
  M.select(function(opts)
    vim.api.nvim_set_current_buf(opts.original_buf)
    vim.api.nvim_win_set_cursor(0, opts.original_pos)
    vim.fn.setreg(reg or '"', vim.fn.join(opts.files, [[\n]]))
  end)
end

return M
