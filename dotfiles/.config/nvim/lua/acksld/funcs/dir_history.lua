local M = {}

local history = {
  global = {},
  tabpage = {},
  window = {},
}

M.on_dir_changed = function()
  local event = vim.v.event
  if event.window_changed then
    return
  end
  local scope = event.scope
  local cwd = event.cwd
  if scope == 'global' then
    table.insert(history.global, cwd)
  elseif scope == 'tabpage' then
    local tabpagenr = vim.fn.tabpagenr()
    if history.tabpage[tabpagenr] == nil then
      history.tabpage[tabpagenr] = {}
    end
    table.insert(history.tabpage[tabpagenr], cwd)
  elseif scope == 'window' then
    local winnr = vim.fn.winnr()
    if history.window[winnr] == nil then
      history.window[winnr] = {}
    end
    table.insert(history.window[winnr], cwd)
  end
end

local last_dir = function()
end

return M
