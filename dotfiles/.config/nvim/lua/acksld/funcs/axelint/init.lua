local ts = vim.treesitter

local ns_id = vim.api.nvim_create_namespace('axellint')

local set_diagnostic = function(bufnr, opts)
  local diagnostics = {}
  for _, opt in ipairs(opts) do
    table.insert(diagnostics, {
      bufnr = bufnr,
      lnum = opt.lnum,
      col = 0,
      severity = vim.diagnostic.severity.HINT,
      message = opt.message,
      source = 'axelint',
    })
  end
  vim.diagnostic.set(ns_id, bufnr, diagnostics, {})
end

local get_root = function(parser)
  local tree = parser:parse()
  return tree[1]:root()
end

local run_checks = function(parser, bufnr, checks)
  local diagnostics = {}
  local root = get_root(parser)
  if type(checks) == 'function' then
    checks = checks()
  end
  for _, check in ipairs(checks) do
    check(root, bufnr, diagnostics)
  end
  set_diagnostic(bufnr, diagnostics)
end

local setup_buffer = function(group, opts)
  local bufnr = opts.bufnr or 0
  local lang = vim.bo.filetype -- TODO can lang be different than ft?
  local success, parser = pcall(ts.get_parser, bufnr, lang)
  if not success then
    vim.notify(string.format("couldn't load get parser for %s, skipping axellint", lang))
    return
  end
  local event = opts.event or {'BufEnter', 'BufWritePost'}
  vim.api.nvim_create_autocmd(event, {
    callback = function()
      -- TODO this blocks the editor :/ how to fix
      run_checks(parser, bufnr, opts.checks)
      -- vim.loop.new_thread({}, function()
      --   run_checks(parser, bufnr, opts.checks)
      -- end)
    end,
    buffer = bufnr,
    group = group,
    desc = 'run custom linter checks',
  })
end

local setup = function(opts)
  local group = vim.api.nvim_create_augroup('AxeLint', {clear = true})
  for ft, checks in pairs(opts.checks) do
    local ft_group = vim.api.nvim_create_augroup('AxeLint' .. ft, {clear = true})
    vim.api.nvim_create_autocmd('FileType', {
      callback = function()
        setup_buffer(ft_group, {
          event = opts.event,
          checks = checks,
        })
      end,
      pattern = ft,
      group = group,
      desc = 'setup buffer local custom linter',
    })
  end
end

return {
  setup = setup,
}
