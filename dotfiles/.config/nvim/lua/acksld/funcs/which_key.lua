local M = {}

M.register = function(...)
  local status, wk = pcall(require, "which-key")
  if not (status) then
    -- vim.notify("couldn't load which-key, skipping mappings")
    return
  end
  wk.register(...)
end

return M
