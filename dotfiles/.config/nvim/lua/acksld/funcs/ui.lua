local M = {}

M.yn = function(opts)
  opts = opts or {}
  vim.ui.select(
    {true, false},
    {
      prompt=opts.prompt or "yes or no?",
      format_item = function(item)
        if item then return "yes" else return "no" end
      end
    },
    function(choice)
      if choice then
        if opts.on_yes then
          opts.on_yes()
        end
      else
        if opts.on_no then
          opts.on_no()
        end
      end
    end
  )
end

return M
