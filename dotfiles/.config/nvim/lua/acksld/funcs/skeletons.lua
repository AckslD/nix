local M = {}

M.setup = function(skeletons)
  local group = vim.api.nvim_create_augroup('MySkeletons', {clear = true})
  for pattern, filename in pairs(skeletons) do
    vim.api.nvim_create_autocmd('BufNewFile', {
      pattern=pattern,
      command='0r ~/.config/nvim/skeletons/' .. filename,
      group=group,
    })
  end
end

return M
