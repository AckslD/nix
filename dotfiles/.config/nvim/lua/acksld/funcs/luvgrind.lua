-- from https://gist.github.com/ii14/b7f77001e1416fef726b5e822c549a17
-- for this to be complete, we'd also need to patch
-- luv metatables, patch async callbacks and keep
-- track of file descriptors

local uv = vim.loop
local handles = setmetatable({}, { __mode = 'k' })

local function install()
  local function pack(...)
    return {...}, select('#', ...)
  end

  for _, name in ipairs({
    'spawn',
    'new_timer',
    'new_prepare',
    'new_check',
    'new_async',
    'new_poll',
    'new_socket_poll',
    'new_signal',
    'new_tcp',
    'new_pipe',
    'new_fs_event',
    'new_fs_poll',
    'new_work',
    'new_thread',
    -- TODO: fs stuff
  }) do
    local fn = assert(uv[name], tostring(name))
    uv[name] = function(...)
      local res, n = pack(fn(...))
      local handle = res[1]
      if type(handle) == 'userdata' then
        handles[handle] = debug.traceback():gsub('\t', '    ')
      end
      return unpack(res, 1, n)
    end
  end
end

local function active()
  for handle, traceback in pairs(handles) do
    local success, result = pcall(uv.is_closing, handle)
    if success and not result then
      print(traceback)
    else
      handles[handle] = nil
    end
  end
end

return {
  install = install, -- patch luv functions
  active = active, -- print active handles
}
