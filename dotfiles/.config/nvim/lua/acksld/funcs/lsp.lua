return {
  get_on_attach = function(lsp)
    return function(client, bufnr) -- {{{
      local opts = { noremap=true, silent=true }

      local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
      local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

      buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
      if client.server_capabilities.documentSymbolProvider then
        IfHas('nvim-navbuddy', function(navbuddy)
          navbuddy.attach(client, bufnr)
        end)
      end
      IfHas('lsp_signature', function(lsp_signature)
        lsp_signature.on_attach({
            bind = true, -- This is mandatory, otherwise border config won't get registered.
            floating_window = false, -- show hint in a floating window, set to false for virtual text only mode
          })
      end)

      -- Mappings. {{{
      local telescope_or_builtin = function(name, telescope_name)
        if not telescope_name then
          telescope_name = string.format('lsp_%s', name)
        end
        local success, telescope_builtin = pcall(require, 'telescope.builtin')
        if success then
          telescope_builtin[telescope_name]({on_no_result = function()
            vim.notify(string.format("couldn't find %s", name))
          end})
        else
          vim.lsp.buf[name]()
        end
      end
      local keymap = {
        l = {
          name = "+lsp",
          u = {'<Cmd>LspStart<CR>', 'start'},
          i = {function() telescope_or_builtin('implementation', 'lsp_implementations') end, 'implementation'},
          -- i = {'<Cmd>lua vim.lsp.buf.implementation()<CR>', 'implementation'},
          t = {function() telescope_or_builtin('type_definition', 'lsp_type_definitions') end, 'type definition'},
          -- t = {'<Cmd>lua vim.lsp.buf.type_definition()<CR>', 'type definition'},
          h = {function() telescope_or_builtin('references') end, 'references'},
          -- r = {'<Cmd>lua vim.lsp.buf.references()<CR>', 'references'},
          s = {function() telescope_or_builtin('incoming_calls') end, 'incoming calls'},
          -- s = {'<Cmd>lua vim.lsp.buf.incoming_calls()<CR>', 'incoming'},
          S = {function() telescope_or_builtin('outgoing_calls') end, 'outcoming calls'},
          -- S = {'<Cmd>lua vim.lsp.buf.outgoing_calls()<CR>', 'outgoing'},
          o = {function() telescope_or_builtin('document_symbol', 'lsp_document_symbols') end, 'symbols'},
          -- o = {'<Cmd>lua vim.lsp.buf.document_symbol()<CR>', 'symbols'},
          -- workspace
          a = {'<Cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', 'add workspace'},
          R = {'<Cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', 'remove workspace'},
          l = {'<Cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', 'list workspace'},
          -- code action
          q = {"<Cmd>lua vim.lsp.buf.code_action()<CR>", 'code action'},
        },
        r = {
          name = "+refactor",
          -- rename
          n = {"<Cmd>lua vim.lsp.buf.rename()<CR>", 'rename (lsp)'},
        },
      }
      local visual_keymap = {l = {
        name = "+lsp",
        q = {"<Cmd>'<,'>lua vim.lsp.buf.range_code_action()<CR>", 'code action'},
      }}
      -- Set some keybinds conditional on server capabilities
      if client.server_capabilities.documentFormattingProvider then
          keymap.l.f = {"<Cmd>lua vim.lsp.buf.format({async = false})<CR>", 'format'}
      end
      -- if client.server_capabilities.documentRangeFormattingProvider then
      --     visual_keymap.l.f = {"<Cmd>lua vim.lsp.buf.range_formatting()<CR>", 'format'}
      -- end
      WK(keymap, {prefix = '<leader>', buffer = bufnr})
      WK(visual_keymap, {prefix = '<leader>', buffer = bufnr, mode = 'v'})

      WK({
        -- preview definition
        d = {function() telescope_or_builtin('definition', 'lsp_definitions') end, 'lsp definition'},
        D = {"<Cmd>lua vim.lsp.buf.declaration()<CR>", 'lsp declaration'},
      }, {prefix = 'g', buffer=bufnr})

      -- show hover doc
      buf_set_keymap('n', 'K', "<Cmd>lua vim.lsp.buf.hover()<CR>", opts)
      vim.keymap.set('i', '<C-s>', vim.lsp.buf.signature_help, {desc = 'signature help'})
      -- }}}

      -- Set autocommands conditional on server_capabilities
      if client.server_capabilities.documentHighlightProvider then
        R('funcs.autocmds').create_grouped_autocmds({
          MyLSPHighlightClear = {
            CursorMoved = {
              callback = vim.lsp.buf.clear_references,
              buffer = 0,
            },
          },
        })
      end
      local jedi_capabilities = {
        hoverProvider = true,
        -- completionProvider = true,
      }
      if client.name == 'pyright' then
        for jedi_capability, _ in pairs(jedi_capabilities) do
          client.server_capabilities[jedi_capability] = false
        end
      end
      if client.name == 'jedi_language_server' then
        for capability, _ in pairs(client.server_capabilities) do
          if capability:find('%w*Provider') and not jedi_capabilities[capability] then
            client.server_capabilities[capability] = false
          end
        end
      end
      if client.name == 'pylyzer' then
        for capability, _ in pairs(client.server_capabilities) do
          if capability:find('%w*Provider') then
            client.server_capabilities[capability] = false
          end
        end
      end
      if lsp == 'efm' and vim.o.filetype == 'python' then
        client.resolved_capabilities.document_formatting = false
        client.resolved_capabilities.document_range_formatting = false
      end
    end -- }}}
  end,
}
