local M = {}

M.pager = function()
  vim.api.nvim_create_autocmd('TermOpen', {
    callback = function()
      vim.wo.number = false
      vim.wo.relativenumber = false
      vim.wo.winbar = ''
      require('lualine').hide()
    end
  })
  vim.api.nvim_create_autocmd('VimLeavePre', {
    callback = function()
      vim.cmd('silent !rm /tmp/kitty_scrollback_buffer')
    end,
  })
end

return M
