local M = {}

local PATTERNS = {
  case = '^_+ [%w_]+%[.*',
  error = '^[%w/_]+%.py:%d+:',
}

local parse = function(bufnr)
  local lines = vim.api.nvim_buf_get_lines(bufnr or 0, 1, -1, false)
  local matches = {}
  for i, line in ipairs(lines) do
    for name, pattern in pairs(PATTERNS) do
      if line:match(pattern) then
        table.insert(matches, {
          line = line,
          name = name,
          lnum = i,
        })
      end
    end
  end
  return matches
end

local TYPES = {
  case = 'T',
  error = 'E',
}

M.set_qf_list = function(bufnr)
  local matches = parse(bufnr)
  local loc_items = {}
  for _, match in ipairs(matches) do
    table.insert(loc_items, {
      bufnr = bufnr or vim.fn.bufnr(),
      lnum = match.lnum + 1,
      text = match.line,
      type = TYPES[match.name],
    })
  end
  vim.fn.setqflist(loc_items)
end

return M
