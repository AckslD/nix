local M = {}

M.define_commands = function(commands)
  for name, opts in pairs(commands) do
    local command = opts.command
    opts.command = nil
    if opts.buffer == nil then
      vim.api.nvim_create_user_command(name, command, opts)
    else
      local buffer = opts.buffer
      opts.buffer = nil
      vim.api.nvim_buf_create_user_command(buffer, name, command, opts)
    end
  end
end

return M
