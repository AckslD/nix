return function()
  IfHas("which-key", function(wk)
    wk.register({
      r = {'<Cmd>source %<CR>', 'source file'},
      t = {"<Plug>PlenaryTestFile", "test file"},
    }, {prefix = '<localleader>', buffer = 0})
  end)
end
