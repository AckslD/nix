local function py(func)
  return function(opts)
    return R("funcs.ft.python")[func](opts)
  end
end

return function()
    -- run and test
    R('funcs.dedterm').setup{
        run = {
            cmd = "p",
            post_args = "%:p"
        },
        test = {
            cmd = "p -m pytest --tb=short",
            post_args = "%:p",
            after = function()
              IfHas('which-key', function(wk) wk.register({
                d = {function()
                  vim.cmd("cgetbuffer")
                  vim.cmd.normal('gg')
                  vim.fn.search('^_')
                  vim.cmd.PytrizeJump()
                  -- center cursor
                  -- XXX hangs nvim
                  -- vim.cmd.normal('zz')
                  vim.cmd.normal('mm')
                  vim.cmd.normal('G')
                  vim.cmd.normal('`m')
                  vim.cmd('copen')
                end, 'debug first'},
              }, {prefix = '<localleader>', buffer = 0}) end)
              vim.o.errorformat = ''..
                  [[%E%f:%l:\ in\ %o,]]..
                  [[%C\ %\{4}%m,]]..
                  [[%+G_%\+ %.%#,]]..
                  [[%+GDuring handling of the above exception\, another exception occurred:,]]..
                  [[%+GE\ %.%#,]]..
                  [[%Z%\S%.%#,]]..
                  [[%-C%.%#,]]..
                  [[%-G%.%#]]
            end,
        },
    }

    -- run and test using dap to debug
    vim.api.nvim_create_user_command('RunModule', py('run_module'), {nargs = 0})
    vim.api.nvim_create_user_command('RunDebug', py('run_debug'), {nargs = '*'})
    vim.api.nvim_create_user_command('TestDebug', py('test_debug'), {nargs = '*'})

    local keymap = {
      j = {
        function()
          R("funcs.terminal").dedicated(
            "p -m jupyter notebook %",
            {termname = 'jupyter', precmd = "tabnew"}
          )
        end,
        'open jupyter',
      },
      f = {py('test_function'), 'test function'},
      p = {py('test_cwd'), 'test cwd'},
      R = {py('run_debug'), 'run (dap)'},
      T = {py('test_debug'), 'test (dap)'},
      F = {py('test_function_debug'), 'test function (dap)'},
      P = {py('test_cwd_debug'), 'test cwd (dap)'},
      s = {py('toggle_f_string'), 'toggle f-string'},
      i = {py('split_from_import'), 'split from import'},
      u = {py('remove_unused_imports'), 'remove unused imports'},
      k = {py('func_kwarg_to_dict_entry'), 'kwarg -> key'},
      K = {py('dict_entry_to_func_kwarg'), 'key -> kwarg'},
      q = {function()
        R('funcs.pytest').set_qf_list()
        vim.cmd('cfirst')
        vim.cmd('copen')
      end, 'local qf list'},
      m = {py('ignore_mypy'), 'ignore mypy'},
      M = {py('remove_ignore_mypy'), 'remove ignore mypy'},
      a = {function() require('apollo').toggle() end, 'toggle apollo injection'},
      l = {
        a = {function()
          R('funcs.keep').cursor_wrap(function()
           vim.cmd('%! autopep8 -')
          end)()
        end, 'autopep8'},
        i = {function()
          R('funcs.keep').cursor_wrap(function()
           vim.cmd('%! isort - -d --sp ~/.isort.cfg')
          end)()
        end, 'isort'},
      },
    }
    IfHas('which-key', function(wk) wk.register(keymap, {prefix = '<localleader>', buffer = 0}) end)
    -- TODO fix how to access the range
    IfHas('which-key', function(wk) wk.register({
      k = {py('func_kwarg_to_dict_entry'), 'kwarg -> key'},
      K = {py('dict_entry_to_func_kwarg'), 'key -> kwarg'},
    }, {mode = 'v', prefix = '<localleader>', buffer = 0}) end)
    IfHas('plugin_settings.ipy', function(ipy) ipy.setup() end)

    vim.bo.formatprg=[[autopep8 - | isort -d -]]
    -- vim.opt_local.indentkeys:remove({'<:>'})

    vim.bo.makeprg = [[python\ %]]
    vim.bo.errorformat = ''..
        [[%*\sFile "%f"\, line %l\, %m,]]..
        [[%*\sFile "%f"\, line %l,]]..
        [[%f:%l: %m,]]..
        [[%f:%l:,]]..
        [[%-G%.%#]]
end
