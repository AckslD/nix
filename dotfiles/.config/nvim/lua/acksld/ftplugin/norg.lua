return function()
    IfHas('which-key', function(wk) wk.register({
      c = {'<Cmd>Neorg gtd capture<CR>', 'capture'},
      e = {'<Cmd>Neorg gtd edit<CR>', 'edit'},
      v = {'<Cmd>Neorg gtd views<CR>', 'views'},
      n = {'<Cmd>Neorg news all<CR>', 'news'},
      p = {'<Cmd>Neorg presenter start<CR>', 'start'},
      P = {'<Cmd>Neorg presenter close<CR>', 'close'},
      w = {'<Cmd>Neorg workspaces<CR>', 'workspaces'},
    }, {prefix = '<localleader>', buffer = 0}) end)
end
