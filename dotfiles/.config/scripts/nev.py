import sys
from subprocess import Popen
from subprocess import PIPE

from manven.commands import list_environments
from manven.commands import create_environment
from manven.commands import activate_environment
from manven.commands import activate_temp_environment

pick_clone = False
if '-p' in sys.argv:
    pick_clone = True
    sys.argv.remove('-p')

if len(sys.argv) < 2:
    name = None
else:
    name = sys.argv[1]

current = list_environments(include_temporary=True)
if name in current:
    try:
        input(f'"{name}" already exists, press enter to continue or ctrl-c to cancel')
    except KeyboardInterrupt:
        print('\nAborting!')
        exit(1)

# query clone env
if pick_clone:
    p = Popen('fzf --prompt="Pick a base venv to use if any: "', shell=True, stdin=PIPE, stdout=PIPE)
    stdout, stderr = p.communicate('\n'.join(current).encode())
    answer = stdout.decode('utf-8').strip()
    if answer:
        clone = answer
    else:
        clone = None
else:
    clone = 'base'

if name:
    create_environment(name, replace=True, clone=clone)
    activate_environment(name)
else:
    activate_temp_environment(clone=clone)
