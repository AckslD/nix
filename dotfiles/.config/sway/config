# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term $TERM_EMULATOR
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
set $menu rofi -show run | xargs swaymsg exec --

### Settings
default_border pixel
default_floating_border pixel 1
client.focused #fe8019 #fe8019 #282828
gaps inner 1
gaps outer 1
smart_gaps on
smart_borders on
# font monospace 0

#### Idle
exec swayidle -w \
	timeout 1800 'swaylock' \
	timeout 1805 'swaymsg "output * dpms off"' \
	resume 'swaymsg "output * dpms on"' \
	before-sleep 'playerctl pause' \
	before-sleep 'keyringman lock' \
	before-sleep 'swaylock'

#### Input
input type:touch events disabled
input type:touchpad {
    dwt enabled
    tap enabled
}
input type:keyboard {
    repeat_delay 200
    repeat_rate 30
}

#### Output
output * bg ~/pictures/background.jpg fill
exec way-displays > /tmp/way-displays.${XDG_VTNR}.${USER}.log 2>&1
# Example
# output HDMI-A-1 resolution 1920x1080 position 1920,0

### Startup
# To share screen (working?)
exec dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway
exec systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP

# Things to start up after graphical
exec . ~/.profile
exec wl-paste -t text --watch myclipman

### Criterias
for_window [app_id="floating"] floating enable
for_window [app_id="onboard"] {
    floating enable
    sticky enable
    resize set 100ppt 30ppt
    move position 0ppt 70ppt
}
for_window [app_id="zoom"] move window to workspace 4
for_window [title="as_toolbar" app_id="zoom"] floating enable; sticky enable; move position 25 ppt 0 ppt

### Idle configuration
#
# Example configuration:
#
# exec swayidle -w \
#          timeout 300 'swaylock -f -c 000000' \
#          timeout 600 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
#          before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

### Input configuration
#
# Example configuration:
#
#   input "2:14:SynPS/2_Synaptics_TouchPad" {
#       dwt enabled
#       tap enabled
#       natural_scroll enabled
#       middle_emulation enabled
#   }
#
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

### Key bindings
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Return exec $term
    bindsym $mod+t exec $term

    # Kill focused window
    bindsym $mod+Shift+c kill

    # Special keys
    # Audio
    bindsym --locked XF86AudioMute exec amixer -D pulse set Master toggle
    bindsym --locked XF86AudioMicMute exec amixer -D pulse set Capture toggle
    bindsym --locked XF86AudioLowerVolume exec amixer -D pulse set Master 5%-
    bindsym --locked XF86AudioRaiseVolume exec amixer -D pulse set Master 5%+
    # Backlight
    bindsym XF86MonBrightnessUp exec xbacklight -inc 10
    bindsym XF86MonBrightnessDown exec xbacklight -dec 10
    # Bluetooth
    bindsym XF86Bluetooth exec bluetooth toggle
    # Language
    bindsym XF86Tools exec (setxkbmap -query | grep -E 'layout:\s+us') && setxkbmap se || setxkbmap us

    # general
    bindsym $mod+Shift+q exec zsh -c 'prompt-yn "Shutdown computer?" -c "shutdown now" -w'
    bindsym $mod+Shift+r exec zsh -c 'prompt-yn "Restart computer?" -c "shutdown -r now" -w'
    bindsym $mod+Shift+i exec qutebrowser
    bindsym Control+Shift+l exec zsh -c "lockAndSleep --sleep"
    bindsym Control+Alt+l exec zsh -c lockAndSleep
    bindsym $mod+Shift+s exec $term -e spt
    bindsym $mod+m exec $EMAIL_CLIENT
    bindsym $mod+Shift+m exec $MESSAGE_CLIENT
    bindsym $mod+e exec rofi -show run | xargs swaymsg exec --
    bindsym $mod+Shift+e exec rofi -show drun | xargs swaymsg exec --
    bindsym $mod+w exec rofi -show window
    # bindsym $mod+Shift+w exec swaymsg -t get_tree | jq -r '..|try select(.focused == true)' | swaynag -l -m 'Current window data' -t warning
    bindsym $mod+Shift+w exec swaymsg -t get_tree | swaynag -l -m 'Current window data' -t warning
    bindsym $mod+n exec zsh -c "wifiman -f -w"
    bindsym $mod+o exec $term --title="fzfopen" --class="floating" -e zsh -c "fzfopen"
    bindsym $mod+d exec $term --title="vimtodo" --class="floating" -e zsh -c "vimtodo"
    bindsym $mod+Shift+d exec $term --title="vimnoteman" --class="floating" -e zsh -c "vimnoteman"

    # notifications
    bindsym $mod+q exec dunstctl close-all

    # clipboard
    bindsym $mod+x exec zsh -c 'clipman pick -t rofi'
    bindsym $mod+Shift+x exec $term --title="vimxclip" --class="floating" -e zsh -c "vimxclip"
    bindsym $mod+Shift+b exec zsh -c 'khard email | rofi -dmenu | sed "s/^.*\s\(\S\+\)\s*$/\1/" | xsel --clipboard'

    # emojis
    bindsym $mod+b exec rofimoji

    # pomodoro
    bindsym $mod+a exec zsh -c "pomodoro start"
    bindsym $mod+Shift+a exec zsh -c "pomodoro sb"
    bindsym $mod+Shift+Control+a exec zsh -c "pomodoro lb"
    bindsym $mod+s exec zsh -c "pomodoro-cli quit"

    # passman
    bindsym $mod+y exec zsh -c "kpdb"
    bindsym $mod+u exec zsh -c "passman clip -w"
    bindsym $mod+Shift+u exec zsh -c "passman clip --username --window"

    # Print screen
    bindsym Print exec gscreenshot-cli -c -f ~/pictures/screenshots
    bindsym Shift+Print exec gscreenshot-cli -s -c -f ~/pictures/screenshots

    # Spotify
    bindsym --locked $mod+Left        exec spt pb --previous
    bindsym --locked $mod+Right       exec spt pb --next
    bindsym --locked $mod+Up          exec spt pb --shuffle
    bindsym --locked $mod+Down        exec spt pb --toggle
    bindsym --locked $mod+Shift+Left  exec playerctl --all-players previous
    bindsym --locked $mod+Shift+Right exec playerctl --all-players next
    bindsym --locked $mod+Shift+Up    exec playerctl --all-players play
    bindsym --locked $mod+Shift+Down  exec playerctl --all-players pause

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+c reload

    # Exit sway (logs you out of your Wayland session)
    # bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'
#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    bindsym $mod+Tab focus next
    # Or use $mod+[up|down|left|right]
    # bindsym $mod+Left focus left
    # bindsym $mod+Down focus down
    # bindsym $mod+Up focus up
    # bindsym $mod+Right focus right

    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    bindsym $mod+Ctrl+Shift+$left move workspace to output left
    bindsym $mod+Ctrl+Shift+$down move workspace to output down
    bindsym $mod+Ctrl+Shift+$up move workspace to output up
    bindsym $mod+Ctrl+Shift+$right move workspace to output right
    # Ditto, with arrow keys
    # bindsym $mod+Shift+Left move left
    # bindsym $mod+Shift+Down move down
    # bindsym $mod+Shift+Up move up
    # bindsym $mod+Shift+Right move right
#
# Workspaces:
#
    # Switch to workspace
    bindsym $mod+1 workspace number 1
    bindsym $mod+2 workspace number 2
    bindsym $mod+3 workspace number 3
    bindsym $mod+4 workspace number 4
    bindsym $mod+5 workspace number 5
    bindsym $mod+6 workspace number 6
    bindsym $mod+7 workspace number 7
    bindsym $mod+8 workspace number 8
    bindsym $mod+9 workspace number 9
    bindsym $mod+0 workspace number 10
    # bindsym $mod+comma workspace prev
    # bindsym $mod+period workspace next
    bindsym $mod+comma exec ~/.config/sway/scripts/prev
    bindsym $mod+period exec ~/.config/sway/scripts/next
    # Move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6
    bindsym $mod+Shift+7 move container to workspace number 7
    bindsym $mod+Shift+8 move container to workspace number 8
    bindsym $mod+Shift+9 move container to workspace number 9
    bindsym $mod+Shift+0 move container to workspace number 10
    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.
#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+Control+Shift+s splith
    bindsym $mod+Control+Shift+v splitv

    # Cycle layouts
    bindsym $mod+slash layout toggle splith splitv tabbed
    # Switch the current container between different layout styles
    # bindsym $mod+s layout stacking
    # bindsym $mod+w layout tabbed
    # bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+f floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+shift+p focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+p focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+z mode "resize"

#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
    swaybar_command waybar
    # position top

    # When the status_command prints a new line to stdout, swaybar updates.
    # The default just shows the current date and time.
    # status_command waybar

    # colors {
    #     statusline #ffffff
    #     background #323232
    #     inactive_workspace #32323200 #32323200 #5c5c5c
    # }
}

include /etc/sway/config.d/*
