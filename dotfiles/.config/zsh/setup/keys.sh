# vifm
bindkey -s '^n' 'vicd .\n'
# fzf
bindkey -s '^p' 'vimfzf\n'
# livegrep (fzf & rg)
bindkey -s '^f' 'lrg\n'
# notes
bindkey -s '^o' 'noteman\n'
# todos
bindkey -s '^d' 'gtd-capture\n'
# Reverse tab
bindkey '^[[Z' reverse-menu-complete

# completion
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
