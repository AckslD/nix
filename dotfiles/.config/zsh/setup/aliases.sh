###########
# general #
###########
alias v=nvim
if hash neovide 2> /dev/null; then
  # alias nv="neovide --multigrid"
  alias nv=nvim
else
  alias nv=nvim
fi
alias vs="nvim -S Session.vim"
alias vim=nvim
alias gtd="nvim ~/todo/todo.txt"
alias gtdi="nvim ~/todo/QuickNote.md"
# TODO
alias gtd-capture=gtdi
alias gtd-today=gtd
# alias gtd-capture="nvim ~/todo/neorg/inbox.norg '+Neorg gtd capture'"
# alias gtd-today="nvim ~/todo/neorg/index.norg '+Neorg gtd views' '+normal gst'"
alias sv="nvim --noplugin -u ~/.config/nvim/secure.vim"
alias ls=exa
alias open=xdg-open
if hash pyenv 2> /dev/null; then
    alias smanven='source $(pyenv which manven)'
else
    alias smanven='source $(which manven)'
fi
alias nev='source ~/.config/scripts/nev.sh'
alias hc=herbstclient

# stack
alias d='dirs -v'
for index ({1..9}) alias "$index"="cd +${index}"; unset index

#######
# fzf #
#######
alias fzf_files='fzf --preview "bat {} --color=always"'
alias fzf_noteman='noteman -l | fzf --preview "bat {} --color=always"'
alias vimnoteman='fzf_noteman | xargs -r nvim'
alias lrg="eval \$(livegrep | awk -F: '{print \"nvim \" \$1 \" +\" \$2}')"
alias nz='zi'  # zoxide
alias pipls='pip list --format json 2> /dev/null | jq ".[].name" -r | fzf'
alias pips='pipls | xargs -r pip show'

###########
# ripgrep #
###########
alias rgh='batgrep'

#######
# feh #
#######
alias feeh='feh --draw-filename --fullscreen --scale-down --auto-zoom --image-bg black'

#########
# latex #
#########
alias pdflatex='pdflatex $TEX_FLAGS'

########
# vifm #
########
alias vf='vifmrun'
alias vifm='vifmrun'

#######
# git #
#######
if [[ $(alias gg > /dev/null) ]]; then
    unalias gg
fi
alias lg="lazygit"
alias gs="git status"
alias gd="git diff"
alias gdt="git difftool"
alias gp="git pull"
alias gl="git log --all --oneline --graph"
alias gc='git clone'
alias gcb='git clone --bare'
# Git prune remote
alias gpr="git remote update origin --prune"
# mr
alias mrstatus="mr status | pcregrep -M '^mr status.*\$(\\n^.*\\S.*\$)+\\n^\$\\n'"

##########
# pacman #
##########
alias pupdate="sudo pacman -Syy && sudo pacman -Syu"
alias pforeign="sudo pacman -Qqm"
alias update-mirrors="sudo reflector --verbose -p https -l 200 --score 20 --sort rate --save /etc/pacman.d/mirrorlist"

###########
# keyring #
###########
alias lock-keyring='dbus-send --dest=org.gnome.keyring --print-reply /org/freedesktop/secrets org.freedesktop.Secret.Service.LockService'

#######
# w3m #
#######
alias socksifyw3m='socksify w3m -T text/html -cols $(tput cols) -o display_link_number=true'

#############
# systemctl #
#############
alias sstart="systemctl start"
alias srestart="systemctl restart"
alias sstop="systemctl stop"
alias senable="systemctl enable"
alias sdisable="systemctl disable"
alias sstatus="systemctl status"
alias sreload="systemctl daemon-reload"
alias sustart="systemctl start --user"
alias surestart="systemctl restart --user"
alias sustop="systemctl stop --user"
alias sustatus="systemctl status --user"
alias suenable="systemctl enable --user"
alias sudisable="systemctl disable --user"

#############
# bluetooth #
#############
alias btc_headphones="connect_bt_device E4:22:A5:37:C6:D7"
alias btd_headphones="bluetoothctl disconnect E4:22:A5:37:C6:D7"
alias btc_boom="connect_bt_device 88:C6:26:03:AE:60"
alias btd_boom="bluetoothctl disconnect 88:C6:26:03:AE:60"
alias btc_xm4="connect_bt_device 88:C9:E8:F9:13:76"
alias btd_xm4="bluetoothctl disconnect 88:C9:E8:F9:13:76"

###########
# openvpn #
###########
alias ovci='openvpn3 config-import'                           # Import configuration profiles
alias ovcm='openvpn3 config-manage'                           # Manage configuration properties
alias ovca='openvpn3 config-acl'                              # Manage access control lists for configurations
alias ovcs='openvpn3 config-show'                             # Show/dump a configuration profile
alias ovcr='openvpn3 config-remove'                           # Remove an available configuration profile
alias ovcl='openvpn3 configs-list'                            # List all available configuration profiles
alias ovss='openvpn3 session-start --config'                  # Start a new VPN session
alias ovsm='openvpn3 session-manage --config'                 # Manage VPN sessions
alias ovsr='openvpn3 session-manage --restart --config'       # Manage VPN sessions
alias ovsd='openvpn3 session-manage --disconnect --config'    # Manage VPN sessions
alias ovsa='openvpn3 session-acl'                             # Manage access control lists for sessions
alias ovst='openvpn3 session-stat --configs'                  # Show session statistics
alias ovsl='openvpn3 sessions-list'                           # List available VPN sessions
alias ovl='openvpn3 log'                                      # Receive log events as they occur
alias ova='openvpn3-autoload --directory ~/.config/openvpn3/autoload'

##############
# expressvpn #
##############
alias evd='expressvpn disconnect'
alias evs='expressvpn status'
alias evl='expressvpn list'
alias evc='expressvpn connect'
alias evcs='expressvpn connect smart'
alias evcse='expressvpn connect Sweden'
alias evcnl='expressvpn connect Netherlands - The Hague'
alias evcus='expressvpn connect USA - New York'
alias evcbr='expressvpn connect Brazil'

########
# wifi #
########
alias wifiscan='iwctl station wlan0 scan'
alias wifishow='iwctl station wlan0 show'
alias wifils='iwctl station wlan0 get-networks'
alias wific='iwctl station wlan0 connect'
alias wifid='iwctl station wlan0 disconnect'

##########
# python #
##########
alias pyv='smanven activate $(smanven list | fzf)'
alias pt='p -m pytest'
alias pipl='pip install -e .'
alias pipq='pip install -e . --no-deps --no-build-isolation'

###########
# jupyter #
###########
alias jpn='p -m jupyter notebook --browser=firefox'
alias jpni='fd -g "*.ipynb" | fzf | xargs -r p -m jupyter notebook --browser=firefox'
alias jptf='p -m jupytext --set-formats ipynb,py'
alias jpts='p -m jupytext --sync'
