import os
from pathlib import Path

for root, folders, files in os.walk('dotfiles'):
    for file in files:
        new = Path(root) / file
        if '.git' in new.parts:
            continue
        current = Path.home() / new.relative_to('dotfiles')
        if any(p in str(current) for p in ['kitty_grab', 'kitty_search', 'zsh-']):
            continue
        if not current.exists():
            print('no', current)
            continue
        # current.unlink()
        try:
            if current.read_text() != new.read_text():
                new.write_text(current.read_text())
                print('diff', current)
        except UnicodeDecodeError:
            pass
