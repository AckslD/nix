{ config, pkgs, lib, ... }:

with lib;
let
  # get the basename from a path
  basename = path: lists.last (strings.splitString "/" (toString path));
  # recursively list files in a path and put them in a set using value of `make_name` as the key
  # opts are merged with {source = file} as the value
  # TODO how to exclude .git stuff?
  # TODO for eg zsh, kitty how to include git (submodules)
  make_files = {
    dir,
    make_name ? file: (path.removePrefix dir file),
    opts ? {}
  }: builtins.listToAttrs (forEach (filesystem.listFilesRecursive dir) (file: {
    name = (make_name file);
    value = {
      source = file;
    } // opts;
  }));
  # create dot-files to symlink based on the files in `./dotfiles`
  # TODO how to symlink barpyrus?
  dot_files = make_files {dir = ./dotfiles;};
  # create bin-files (executable) to symlink based on the files in `./bin`
  bin_files = make_files {
    dir = ./bin;
    make_name = file: ".local/bin/" + (basename file);
    opts = {executable = true;};
  };
in
{
  home.username = "axel";
  home.homeDirectory = "/home/axel";
  home.stateVersion = "23.11";

  home.packages = [
    pkgs.neovim
    # TODO not yet working, part of neovim config?
    # pkgs.nixd
  ];

  home.file = dot_files // bin_files;

  programs.home-manager.enable = true;
}
